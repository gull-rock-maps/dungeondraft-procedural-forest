# Ruby functions to support a procedural map in a Dungeondraft map file
# Version 1.0, Author: u/uchideshi34

# Note that all functions here should be independent of the map config files and simply work off input and output

$permutation_size = 256

module Bezier
	class ControlPoint
		attr_accessor :x, :y

    # @param x [Numeric]
    # @param y [Numeric]
    #
    # Creates a new control point for the Bézier curve
		def initialize(x,y)
			@x = x
			@y = y
		end

		def - (b)
    	self.class.new(self.x - b.x, self.y - b.y)
    end
    def + (b)
    	self.class.new(self.x + b.x, self.y + b.y)
    end

		# @param point [ControlPoint]
		#
    # @return [ControlPoint] A ControlPoint in a new position
    # Moves a controlpoint with relative distance
    def movepoint (point)
			self.class.new(self.x + point.x, self.y + point.y)
		end

		def inspect
			return @x, @y
		end

		# @return [CurvePoint] Returns a ControlPoint, converted to CurvePoint (this is only a naming difference).
		def to_curvepoint
			CurvePoint.new(self.x, self.y)
		end

    # @return [Array]
    # Returns the control point as an array => [x, y]. The Array is fit to be as argument for ControlPoint.new
    def to_a
			[self.x, self.y]
		end
	end

	class CurvePoint < ControlPoint
		# @return [ControlPoint] point coordinates on the Bézier curve.
		def to_controlpoint
			ControlPoint.new(self.x, self.y)
		end
	end

	class Curve
    # defaults
    DeCasteljau = :decasteljau
    Bernstein = :bernstein

    # this should have been instance variable in the first place, correcting '@@...'
    @calculation_method = Bernstein

		@@fact_memoize = Hash.new
		@@binomial_memoize = Hash.new
		@@pascaltriangle_memoize = Hash.new

		# Ye' olde factorial function
		#
		# @param n [Fixnum]
		# @example
		#   > fact(5)
		def self.fact(n)
			@@fact_memoize[n] ||= (1..n).reduce(:*)
    end

		# @param n [Fixnum]
		# @param k [Fixnum]
		# standard 'n choose k'
		def self.binomial(n,k)
			return 1 if n-k <= 0
			return 1 if k <= 0
			@@binomial_memoize[[n,k]] ||= fact(n) / ( fact(k) * fact( n - k ) )
    end

		# Returns the specified line from the Pascal triangle as an Array
		# @return [Array] A line from the Pascal triangle
		# @example
		#   > pascaltriangle(6)
		def self.pascaltriangle(nth_line) # Classic Pascal triangle
			@@pascaltriangle_memoize[nth_line] ||= (0..nth_line).map { |e| binomial(nth_line, e) }
    end

		# Returns the Bezier curve control points
		#
		# @return [Array<ControlPoints>]

		attr_accessor :controlpoints

		# @param controlpoints [Array<ControlPoints>, Array<(Fixnum, Fixnum)>] list of ControlPoints defining the hull for the Bézier curve. A point can be of class ControlPoint or an Array containig 2 Numerics, which will be converted to ControlPoint.
		# @return [Curve] Creates a new Bézier curve object. The minimum number of control points is currently 3.
		# @example
		#    initialize(p1, p2, p3)
		#    initialize(p1, [20, 30], p3)
		def initialize(*controlpoints)

			# need at least 3 control points
			# this constraint has to be lifted, to allow adding Curves together like a 1 point curve to a 3 point curve
			if controlpoints.size < 3
				raise ArgumentError, 'Cannot create curve with less than 3 control points'
			end

			@controlpoints = controlpoints.map { |point|
				if point.class == Array
					ControlPoint.new(*point[0..1]) # ControlPoint.new gets no more than 2 arguments, excess values are ignored
				elsif point.class == ControlPoint
					point
				else
					raise 'Control points should be type of ControlPoint or Array'
				end
			  }
		end

		# Adds a new control point to the Bezier curve as endpoint.
		#
		# @param [ControlPoint, Array] point
		def add(point)
      @controlpoints << case point
                        when ControlPoint
                          point
                        when Array
                          ControlPoint.new(*point[0..1])
                        else
                          raise(TypeError, 'Point should be type of ControlPoint')
                        end

			# if point.class == ControlPoint
			# 	@controlpoints << point
   #    elsif point.class == Array
   #      @controlpoints << ControlPoint.new(*point[0..1])
			# else
			# 	raise TypeError, 'Point should be type of ControlPoint'
			# end
		end

		# @param [CurvePoint] t
		def point_on_curve(t) # calculates the 'x,y' coordinates of a point on the curve, at the ratio 't' (0 <= t <= 1)
      case
        when @calculation_method == DeCasteljau

        when @calculation_method == Bernstein
          poc_with_bernstein(t)
        else
          poc_with_bernstein(t) # default
      end
    end
    alias_method :poc, :point_on_curve

		def poc_with_bernstein(t)
			n = @controlpoints.size-1
      sum = [0,0]
      for k in 0..n
      	sum[0] += @controlpoints[k].x * self.class.binomial(n,k) * (1-t)**(n-k) * t**k
      	sum[1] += @controlpoints[k].y * self.class.binomial(n,k) * (1-t)**(n-k) * t**k
      end
      return ControlPoint.new(*sum)
      #poc_with_decasteljau(t)
    end

    def point_on_hull(point1, point2, t) # just realized this was nested (geez), Jörg.W.Mittag would have cried. So it is moved out from poc_with_decasteljau()
      if (point1.class != ControlPoint) or (point2.class != ControlPoint)
        raise TypeError, 'Both points should be type of ControlPoint'
      end
      new_x = (1-t) * point1.x + t * point2.x
      new_y = (1-t) * point1.y + t * point2.y
      return ControlPoint.new(new_x, new_y)
    end

		def point_on_curve_binom(t)
			coeffs = self.class.pascaltriangle(self.order)
			coeffs.reduce do |memo, obj|
				memo += t**obj * (1-t)** (n - obj)
			end
		end


		def gnuplot_hull # was recently 'display_points'. just a helper, for quickly put ControlPoints to STDOUT in a gnuplottable format
			@controlpoints.map{|point| [point.x, point.y] }
		end

		def gnuplot_points(precision)
		end

		# returns a new Enumerator that iterates over the Bezier curve from [start_t] to 1 by [delta_t] steps.
		def enumerated(start_t, delta_t)
	  		Enumerator.new do |yielder|
	  			#TODO only do the conversion if start_t is not Float, Fractional or Bigfloat
	  			point_position = start_t.to_f
	    		number = point_on_curve(point_position)
	    		loop do
	      			yielder.yield(number)
	      			point_position += delta_t.to_f
	      			raise StopIteration if point_position > 1.0
	      			number = point_on_curve(point_position)
	    		end
	  		end
		end

		# returns the order of the Bezier curve, aka the number of control points.
		def order
			@controlpoints.size
		end
	end
end

#Create a vector array of points in a circle around a central point
def make_circle(radius, centre, num_points)

	array = Array.new(){Array.new(2)}
	start = Array.new(2)

	start = [centre[0]+radius,centre[1]]
	array << start
	theta = Math::PI * 2 / num_points

	for i in 1..num_points-1 do
		x = Math.cos(theta*i) * radius + centre[0]
		y = Math.sin(theta*i) * radius + centre[1]

		array << [x,y]
	end

	return array

end

# Shuffle an array
def shuffle(arrayToShuffle)
	for i in 0..arrayToShuffle.length-1
		j = arrayToShuffle.length - i
		index = (rand*(j-1)).round()
		temp = arrayToShuffle[j-1]
		arrayToShuffle[j-1] = arrayToShuffle[index]
		arrayToShuffle[index] = temp
	end
end

#Make Permutation
def makepermutation()
	permutation = Array.new
	for i in 0..$permutation_size-1 do
		permutation.push(i)
	end

	shuffle(permutation)

	for i in 0..$permutation_size-1 do
		permutation.push(permutation[i])
	end

	return permutation
end

def fade(t)
	return ((6*t - 15)*t + 10)*t*t*t
end
def lerp(t, a1, a2)
	return a1 + t*(a2-a1)
end

def grad(hash, x, y)
	h = hash & 15
	u = h < 8 ? x : y
	v = h < 4 ? y : h == 12 || h == 14 ? x : 0
	((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v)
end

def noise2d(x, y)
	cx = x.floor() & ($permutation_size-1)
	cy = y.floor() & ($permutation_size-1)

	xf = x-x.floor()
	yf = y-y.floor()

	u = fade(xf)
	v = fade(yf)

  a = $permutation[cx] + cy
	b = $permutation[cx + 1] + cy

	return lerp(v, lerp(u, grad($permutation[a], xf, yf), grad($permutation[b], xf - 1, yf)),
				 lerp(u, grad($permutation[a + 1], xf, yf - 1), grad($permutation[b + 1], xf - 1, yf - 1)))

end

# Fractal Additional
def fractal(x,y,octaves)
	result = 0.0
	a = 1.0
	f = 0.005
	for i in 0..octaves-1 do
		v = a*noise2d(x*f, y*f)
		result += v
		a *= 0.5
		f *= 2.0
	end
	if result > 1.0 then
		result = 1.0
	elsif result < -1.0 then
		result = -1.0
	end
	return result
end

def createperlin(size,granularity,octaves,permutationsize)
	puts "Making Perlin noise: pixels #{size}, granularity: #{granularity}, octaves: #{octaves}"
	t = Time.now()

	# Create a permutation instance
	$permutation_size = permutationsize
	$permutation = makepermutation()

	pixels = Array.new(size){Array.new(size)}
	for y in 0..size-1 do
		if y % (size*0.1).floor() == 0
			puts "Create pixels: #{y} / #{size}"
		end
		for x in 0..size-1 do

			# Noise2D generally returns a value approximately in the range [-1.0, 1.0]
			n = fractal(x, y, octaves)
			# Transform the range to [0.0, 1.0], supposing that the range of Noise2D is [-1.0, 1.0]
			n += 1.0
			n *= 0.5

			pixels[x][y] = (granularity*n).round()
		end
	end
	puts "Perlin noise creation complete. Time taken: #{(Time.now()-t).floor(2)}s.\n\n"
	return pixels
end

class PoissonDisk
  attr_reader :x_min, :x_max, :y_min, :y_max, :radius, :k, :random, :grid, :point_queue, :first_point

  def initialize(viewport, min_distance, max_tries)
    @x_min = parse(viewport[0], 0)
    @x_max = parse(viewport[1], 0)
    @y_min = parse(viewport[2], 0)
    @y_max = parse(viewport[3], 0)
    @radius = [parse(min_distance, 1), 1].max
    @k = [parse(max_tries, 30), 2].max
    @random = rand
    @grid = {
      width: 0,
      height: 0,
      cell_size: 0,
      data: []
    }
    initialize_parameters
    initialize_state
  end

  def reset
    initialize_state
  end

  def next
    next_point
  end

  def all
    initialize_state
    all_points
  end

  def done
    !first_point && point_queue.empty?
  end

  private

  def parse(x, k)
    Float(x) rescue k
  end

  def initialize_parameters
    grid[:cell_size] = radius * Math.sqrt(1 / 2.0)
    grid[:width] = ((x_max - x_min) / grid[:cell_size]).ceil
    grid[:height] = ((y_max - y_min) / grid[:cell_size]).ceil
    grid[:data] = Array.new(grid[:width] * grid[:height])
  end

  def initialize_state
    @point_queue = []
    @first_point = true
    grid[:data].fill(nil)
  end

  def dist2(x1, y1, x2, y2)
    (x2 - x1)**2 + (y2 - y1)**2
  end

  def create_new_point(x, y)
    point = { x: x, y: y }
    index = (x / grid[:cell_size]).floor + (y / grid[:cell_size]).floor * grid[:width]
    grid[:data][index] = point
    point_queue.push(point)
    point
  end

  def is_valid_point(x, y)
    return false if x < x_min || x > x_max || y < y_min || y > y_max

    col = ((x - x_min) / grid[:cell_size]).floor
    row = ((y - y_min) / grid[:cell_size]).floor

    for i in (col - 2)..(col + 2)
      for j in (row - 2)..(row + 2)
        if i >= 0 && i < grid[:width] && j >= 0 && j < grid[:height]
          idx = i + (j * grid[:width])
          if grid[:data][idx] && dist2(x, y, grid[:data][idx][:x], grid[:data][idx][:y]) <= (radius**2)
            return false
          end
        end
      end
    end

    true
  end

  def next_point
    x, y = 0, 0

    if first_point
      @first_point = false
      x = x_min + (x_max - x_min) * rand()
      y = y_min + (y_max - y_min) * rand()
      return create_new_point(x, y)
    end

    idx, distance, angle = 0, 0, 0

    while point_queue.any?
      idx = (point_queue.length * rand()).to_i
      for i in 0..k-1 do
        distance = radius * (rand() + 1)
        angle = 2 * Math::PI * rand()
        x = point_queue[idx][:x] + distance * Math.cos(angle)
        y = point_queue[idx][:y] + distance * Math.sin(angle)
        return create_new_point(x, y) if is_valid_point(x, y)
      end
      point_queue.delete_at(idx)
    end

    nil
  end

  def all_points
    result = []

    loop do
      point = next_point()
      break if point.nil?

      result.push(point)
    end

    result
  end
end

def finddistancebetweenpoints(point_one,point_two)

	return Math.sqrt((point_one[0].to_f()-point_two[0].to_f())**2 + (point_one[1].to_f()-point_two[1.to_f()])**2)

end

def transposearrayofcoords(twodarray)

	new_array = twodarray.map(&:clone)

	for i in 0..twodarray.length()-1 do
		new_array[i][1] = twodarray[i][0]
		new_array[i][0] = twodarray[i][1]
	end
	return new_array
end

# Function assumes incoming array has 16 areas per map square and outputs for a standard map square definition
def makebeziercurve(array,density,isloop)

  output = Array.new(){Array.new(2)}
  ctrpnts_array = Array.new()
	total_length = 0.0

	if array.length() < 3
		return array
	end

	#If we want a fixed array size output for example for the side of roads, then density is negative as an input
	if density < 0
		num_points = (1.0/(-density)).ceil()
	else
		#Calculate number of points for each arc based on array coordinate length system
		for i in 0..array.length()-2 do
			total_length += finddistancebetweenpoints(array[i+1],array[i])
			
			if i > 9
				break
			end
		end
		num_points = (total_length.to_f()/i * 1.0/density).ceil()
		
	end

	
  #Draw bezier curves to midpoints and in triples
    for i in 0..array.length()-2 do
        if i == 0

            curve_start = [(array[1][0]-array[0][0])*0.5+array[0][0],(array[1][1]-array[0][1])*0.5+array[0][1]]
            curve_middle = [array[1][0],array[1][1]]
            # find mid point and define that as curve end
            curve_end = [(array[2][0]-array[1][0])*0.5+array[1][0],(array[2][1]-array[1][1])*0.5+array[1][1]]
            # Fill in the initial first few points
			start_points = [(finddistancebetweenpoints(array[1],array[0]) * 1.0/density).floor(),1].max()
            for j in 0..start_points do

                temp = [((curve_start[0]-array[0][0])*j*1.0/start_points+array[0][0]),((curve_start[1]-array[0][1])*j*1.0/start_points+array[0][1])]
                output << temp

            end
        elsif i == array.length()-3 && !isloop
            curve_start = curve_end
            curve_middle = [array[i+1][0],array[i+1][1]]
            curve_end = [array[i+2][0],array[i+2][1]]
        elsif i == array.length()-2 && isloop
            curve_start = curve_end
            curve_middle = [array[i+1][0],array[i+1][1]]
            curve_end = [array[0][0],array[0][1]]
        else
            curve_start = curve_end
            curve_middle = [array[i+1][0],array[i+1][1]]
            curve_end = [(array[i+2][0]-array[i+1][0])*0.5+array[i+1][0],(array[i+2][1]-array[i+1][1])*0.5+array[i+1][1]]
        end

        bez = Bezier::Curve.new(curve_start, curve_middle, curve_end)
        for j in 0..num_points-1 do
            output << [bez.point_on_curve(j.to_f()*1.0/num_points).x,bez.point_on_curve(j.to_f()*1.0/num_points).y]
        end

		if i == array.length()-3 && !isloop
			break
		end

    end

    output << [curve_end[0],curve_end[1]]
	return output

end

def calculate_perpendicular_position(x, y, direction, distance)

  # Calculate new x and y coordinates
  new_x = x + distance * Math.cos(direction + Math::PI/2)
  new_y = y + distance * Math.sin(direction + Math::PI/2)

  return [new_x, new_y]
end

def linear_interpolation(a,b,distance)

	return ((b - a) * distance + a).floor()

end

def filter_path(path_array, filter_factor)

    new_path = Array.new(){Array.new(2)}

	if filter_factor <= 1
		return path_array
	end

    new_path << path_array[0]
    for i in 1..path_array.length()-2 do
        if i % filter_factor == 0
            new_path << path_array[i]
        end
    end
    new_path << path_array[-1]

    return new_path

end

def makeperpendicularpath(sourcepath,distance,islefthand,initialangle)

	location = Array.new(2)
	output_array = Array.new(){Array.new(2)}

	if initialangle == nil
		initialangle = Math.atan2(sourcepath[1][1]-sourcepath[0][1],sourcepath[1][0]-sourcepath[0][0])
	end

	if islefthand
		angle = -Math::PI
	else
		angle = 0
	end

	for i in 0..sourcepath.length()-1 do

		if i == 0
			location = calculate_perpendicular_position(sourcepath[i][0],sourcepath[i][1], initialangle + angle, distance)
			output_array << location
		elsif i == sourcepath.length()-1
			direction = Math.atan2(sourcepath[i][1]-sourcepath[i-1][1],sourcepath[i][0]-sourcepath[i-1][0]) + angle
			location = calculate_perpendicular_position(sourcepath[i][0],sourcepath[i][1], direction, distance)
			output_array << location
		elsif i > 0 && i < sourcepath.length()-1
			direction = Math.atan2(sourcepath[i+1][1]-sourcepath[i-1][1],sourcepath[i+1][0]-sourcepath[i-1][0]) + angle
			location = calculate_perpendicular_position(sourcepath[i][0],sourcepath[i][1], direction, distance)
			output_array << location

		end
	end
	return output_array

end

def collapseperlintobinary(perlin,level)

  newarray = Array.new(perlin.length()){Array.new(perlin.length())}

  for i in 0..perlin.length()-1 do
    for j in 0..perlin.length()-1 do
      if perlin[i][j] > level
        newarray[i][j] = 1
      else
        newarray[i][j] = 0
      end
    end
  end
  
  #Remove single element entries as we will never want these in practice.
  for i in 1..newarray.length()-2 do
    for j in 1..newarray.length()-2 do
      if newarray[i][j] != newarray[i-1][j] && newarray[i][j] != newarray[i+1][j] && newarray[i][j] != newarray[i][j+1] && newarray[i][j] != newarray[i][j-1]
        newarray[i][j] = newarray[i-1][j]
      end
    end
  end

  return newarray
end

def calculate_ms_case(flatmap)

  case_array = Array.new(flatmap.length()-1){Array.new(flatmap.length()-1)}

  for i in 0..flatmap.length()-2 do
    for j in 0..flatmap.length()-2 do
      case_array[i][j] = [flatmap[i][j],flatmap[i+1][j],flatmap[i+1][j+1],flatmap[i][j+1]].join()
    end
  end

  return case_array
end

def getnextdirection(last_direction,casetype)

  ms_case = { "0000" => 0,  "0001" => 1, "0010" => 2,    "0011" => 3,
              "0100" => 4,  "0101" => 5,  "0110" => 6,  "0111" => 7,
              "1000" => 8,  "1001" => 9,  "1010" => 10, "1011" => 11,
              "1100" => 12, "1101" => 13, "1110" => 14, "1111" => 15}


  # Data points assuming the 1s are always on the right hand side.
  next_step = { "0000" => "0",  "0001" => "d",  "0010" => "r",  "0011" => "r",
              "0100" => "u",  "0101" => "ud",  "0110" => "u",  "0111" => "u",
              "1000" => "l",  "1001" => "d",  "1010" => "lr", "1011" => "r",
              "1100" => "l", "1101" => "d", "1110" => "l", "1111" => "0"}


  direction = next_step[casetype]
  if casetype == "0101"
    if last_direction == "r"
      direction = "u"
    else
      direction = "d"
    end
  elsif
    casetype == "1010"
      if last_direction == "u"
        direction = "l"
      else
        direction = "r"
      end
  end

  return direction

end

def findcontourstart(case_array,previous_index)

  start = [0,0,"0",false]

  #Check each perimeter until you get a start point with a 1 on the right hand side and a 0 on the left hand side
  for i in 0..case_array.length()-1 do
    if case_array[i][0][0] == "1" && case_array[i][0][1] == "0"
      start = [i,0,"d",true]
      return start
    end
  end
	for i in 0..case_array.length()-1 do
		if case_array[-1][i][1] == "1" && case_array[-1][i][2] == "0"
			start = [case_array.length()-1,i,"l",true]
			return start
		end
	end
	for i in 0..case_array.length()-1 do
		if case_array[case_array.length()-1-i][-1][2] == "1" && case_array[case_array.length()-1-i][-1][3] == "0"
			start = [case_array.length()-1-i,case_array.length()-1,"u",true]
			return start
		end
	end
	for i in 0..case_array.length()-1 do
		if case_array[0][case_array.length()-1-i][3] == "1" && case_array[0][case_array.length()-1-i][0] == "0"
			start = [0,case_array.length()-1-i,"r",true]
			return start
		end
	end


  #Search the whole array for a contour
  for i in previous_index[0]..case_array.length()-2 do
    if i == previous_index[0]
      start_j = previous_index[1]
    else
      start_j = 1
    end
    for j in 1..case_array.length()-2 do
      if !(case_array[i][j] == "1111" || case_array[i][j] == "0000" || case_array[i][j] == "0101" || case_array[i][j] == "1010")
        start = [i,j,getnextdirection("0",case_array[i][j]),false]
				return start
      end
    end
  end

  return start

end

def removecontourfromcasearry(case_array,contour)

	for i in 0..contour.length()-1 do
		if !(contour[i][0] < 0 || contour[i][1] < 0 || contour[i][0] > case_array.length()-1 || contour[i][1] > case_array.length()-1)
			x = contour[i][0]
			y = contour[i][1]

			if case_array[x][y] == "0101"
				if x > contour[i-1][0]
					case_array[x][y] = "1101"
				else
					case_array[x][y] = "0111"
				end
			elsif case_array[x][y] == "1010"
				if y > contour[i-1][1]
					case_array[x][y] = "1110"
				else
					case_array[x][y] = "1011"
				end
			else
				case_array[x][y] = "0000"
			end
		else
			break
		end
	end

end

def findcontour(case_array,filter)

	# contour variable is defined as x,y for the actual map so is 0.25 of array references and offset by [0.25,0.25]
  contour = Array.new(){Array.new(2)}
	#start_array is defined as [startpoint_x, startpoint_x, direction, boolean: isedgecontour]
  start_array = Array.new(4)
	edgestart = Array.new(2)
	offedgestart = Array.new(2)
  last_direction = "0"
  previous_index = [0,0]
	isedgecontour = false
	store_contour = Array.new(){Array.new(2)}
    

  #Start the array off
  start_array = findcontourstart(case_array, [0,0])
  
  if start_array[0] > 0 && start_array[0] < case_array[0].length()-1 && start_array[1] > 0 && start_array[1] < case_array.length()-1
    previous_index[0] = start_array[0]
    previous_index[1] = start_array[1]
  end
  last_direction = start_array[2]
  # If we didn't find a start point then return nil
  if last_direction == "0"
    return nil
  end

	# Make a start to the path from off the edge of the map
	isedgecontour = start_array[3]
	if isedgecontour
		case last_direction
		# Note that "down" is the positive y direction
		when "u"
			edgestart = [(start_array[0]+0.0)/4.0,(start_array[1]+1.0)/4.0]
			offedgestart = [edgestart[0],edgestart[1]+1]
		when "d"
			edgestart = [(start_array[0]+0.0)/4.0,(start_array[1]-1.0)/4.0]
			offedgestart = [edgestart[0],edgestart[1]-1]
		when "r"
			edgestart = [(start_array[0]-1.0)/4.0,(start_array[1]+0.0)/4.0]
			offedgestart = [edgestart[0]-1,edgestart[1]]
		when "l"
			edgestart = [(start_array[0]+1.0)/4.0,(start_array[1]+0.0)/4.0]
			offedgestart = [edgestart[0]+1,edgestart[1]]
		end
		offedgestart[0] += 0.25
		offedgestart[1] += 0.25
		edgestart[0] += 0.25
		edgestart[1] += 0.25
		contour << offedgestart
		contour << edgestart
	end

  contour << [(start_array[0]+1)/4.0,(start_array[1]+1)/4.0]
  x = start_array[0].to_i()
  y = start_array[1].to_i()

  for i in 0..50000 do

    direction = getnextdirection(last_direction,case_array[x][y])
	if direction == "0"
		puts "Error in findcontour function. Marching squares algorithm got lost."
		puts "Blank direction: #{direction}, case_array[x,y]: #{case_array[x][y]}, last_direction: #{last_direction}\t: x,y #{x},#{y}"
		# If we made some progress before getting here then eliminate the start point in particular
		if store_contour.length()>0
			removecontourfromcasearry(case_array,store_contour)
		end
		return nil
	end

    #store the contour so that it can be removed once complete
	store_contour << [x,y]

    last_direction = direction

    case last_direction
    # Note that "down" is the positive y direction
    when "u"
      y -= 1
    when "d"
      y += 1
    when "r"
      x += 1
    when "l"
      x -= 1
    end
    if x < 0 || x > case_array.length()-1 || y < 0 || y > case_array[0].length()-1 || (x == start_array[0] && y == start_array[1])
      if (x == start_array[0] && y == start_array[1])
        
			else
				#Place a point on the edge
				contour << [(x+1)/4.0,(y+1)/4.0]
				case last_direction
				# Note that "down" is the positive y direction
				when "u"
					y -= 4
				when "d"
					y += 4
				when "r"
					x += 4
				when "l"
					x -= 4
				end
				#Place a point a square off the edge
				contour << [(x+1)/4.0,(y+1)/4.0]
      end
      break
    end

		# Only take every nth point
    if i % filter == 0
			contour << [(x+1)/4.0,(y+1)/4.0]
		end
  end

	removecontourfromcasearry(case_array,store_contour)

  return contour
end

#Pass it a point on the boundary and returns the clockwise direction [x,y]
def get_edge_direction(point,boundary)

	direction = [0,0]

	if point[1] == boundary[0]
		if point[0] == boundary[1]
			direction = [0,1]
		elsif point[0] >= boundary[0] && point[0] < boundary[1]
			direction = [1,0]
		else
			puts "Error: x,y: #{point[0]}, #{point[1]},\tboundary: #{boundary[0]}, #{boundary[1]}"
			exit
		end
		return direction
	elsif point[0] == boundary[1]
		if point[1] == boundary[1]
			direction = [-1,0]
		elsif point[1] >= boundary[0] && point[1] < boundary[1]
			direction = [0,1]
		else
			puts "Error: x,y: #{point[0]}, #{point[1]},\tboundary: #{boundary[0]}, #{boundary[1]}"
			exit
		end
		return direction
	elsif point[1] == boundary[1]
		if point[0] == boundary[0]
			direction = [0,-1]
		elsif point[0] > boundary[0] && point[0] <= boundary[1]
			direction = [-1,0]
		else
			puts "Error: x,y: #{point[0]}, #{point[1]},\tboundary: #{boundary[0]}, #{boundary[1]}"
			exit
		end
		return direction
	elsif point[0] == boundary[0]
		if point[1] == boundary[0]
			direction = [0,1]
		elsif point[1] > boundary[0] && point[1] <= boundary[1]
			direction = [0,-1]
		else
			puts "Error: x,y: #{point[0]}, #{point[1]},\tboundary: #{boundary[0]}, #{boundary[1]}"
			exit
		end
		return direction
	end

	return direction

end

def point_in_polygon(polygonarray, point)

	isinpolygon = false

  for i in 0..polygonarray.length()-1 do
		j = (i+1) % polygonarray.length()
		if ( ((polygonarray[i][1]>point[1]) != (polygonarray[j][1]>point[1])) && (point[0] < (polygonarray[j][0]-polygonarray[i][0]) * (point[1]-polygonarray[i][1]) / (polygonarray[j][1]-polygonarray[i][1]) + polygonarray[i][0]) )
       		isinpolygon = !isinpolygon
  		end
	end
  return isinpolygon

end

#Return an array of indices for which polygon is in which
def find_polygon_containers(arrayofpolygons)

	iscontainedin = Array.new(){Array.new()}
	point = Array.new(2)

	for i in 0..arrayofpolygons.length()-1 do
		iscontainedin[i] = []
		index = ((arrayofpolygons[i].length()-1)*0.5).floor()
		point[0] = (arrayofpolygons[i][index+1][0]+arrayofpolygons[i][index][0])*0.5
		point[1] = (arrayofpolygons[i][index+1][1]+arrayofpolygons[i][index][1])*0.5

		for j in 0..arrayofpolygons.length()-1 do
			if i == j
				next
			end
			if point_in_polygon(arrayofpolygons[j], point) then
				iscontainedin[i] << j
			end
		end
	end

	return iscontainedin
end

def merge_polygons(polygon_one,polygon_two,index_one,index_two,link_point_two,isrotationmatched)

	output = Array.new(){Array.new(2)}

	for i in 0..polygon_one.length()-1 do
		if i < index_one
			output << polygon_one[i]
			#puts "Writing point:\t #{output[-1][0].floor(2)},#{output[-1][1].floor(2)}"
		elsif i == index_one
			output << polygon_one[i]
			#puts "Writing point:\t #{output[-1][0].floor(2)},#{output[-1][1].floor(2)}"
			output << link_point_two
			#puts "Writing point:\t #{output[-1][0].floor(2)},#{output[-1][1].floor(2)}"
			for j in 0..polygon_two.length()-1 do
				index = (j + index_two + 1) % polygon_two.length()
				output << polygon_two[index]
				#puts "Writing point P2:\t #{output[-1][0].floor(2)},#{output[-1][1].floor(2)}"
			end
			output << link_point_two
			#puts "Writing point:\t #{output[-1][0].floor(2)},#{output[-1][1].floor(2)}"
			output << polygon_one[i]
			#puts "Writing point:\t #{output[-1][0].floor(2)},#{output[-1][1].floor(2)}"
		else
			output << polygon_one[i]
			#puts "Writing point:\t #{output[-1][0].floor(2)},#{output[-1][1].floor(2)}"
		end
	end

	return output

end

def check_link_polygon_with_other_polygon(polygon_one, polygon_two)

	max_x = 0.0
	start_index_one = -1
	best_point_two = Array.new(2)
	output = Array.new(){Array.new(2)}

	#Find the point in polgon_one with the largest value of x and choose that as the start point
	for i in 0..polygon_one.length()-1 do
		if polygon_one[i][0] > max_x
			start_index_one = i
			max_x = polygon_one[i][0]
		end
	end

	#Find the point on polygon_two with the least value of x that has y = start_polygon_one
	best_index_two = -1
	best_point_two[1] = polygon_one[start_index_one][1]
	best_point_two[0] = 1000.0
	for i in 0..polygon_two.length()-1 do
		j = (i+1) % polygon_two.length()
		if (polygon_two[i][1] <= polygon_one[start_index_one][1] && polygon_two[j][1] >= polygon_one[start_index_one][1]) || (polygon_two[i][1] >= polygon_one[start_index_one][1] && polygon_two[j][1] <= polygon_one[start_index_one][1])
			test_point_two_x = (polygon_two[j][0] - polygon_two[i][0]) * (polygon_one[start_index_one][1] - polygon_two[i][1]) / (polygon_two[j][1] - polygon_two[i][1]) + polygon_two[i][0]
			if test_point_two_x < best_point_two[0] && test_point_two_x > max_x
				best_index_two = i
				best_point_two[0] = test_point_two_x
			end
		end
	end

	if best_index_two > 0 && best_point_two[0] >= max_x
		return [start_index_one,best_index_two,best_point_two]
	elsif best_index_two > 0 && best_point_two[0] < max_x
		return [start_index_one,-2,best_point_two]
	else
		return [start_index_one,-1,best_point_two]
	end
end

def find_polygons_in_ascending_order_max_x(polygon_list,polygons)

	new_order = Array.new(polygon_list.length())
	max_x_array = Array.new(polygon_list.length(),-10.0)

	#Get array of max values
	for i in 0..polygon_list.length()-1 do

		for j in 0..polygons[polygon_list[i]].length()-1 do
			if polygons[polygon_list[i]][j][0] > max_x_array[i]
				max_x_array[i] = polygons[polygon_list[i]][j][0]
			end
		end
	end

	new_order = max_x_array.map.with_index.sort.map(&:last)
	for i in 0..new_order.length()-1 do
		new_order[i] = polygon_list[new_order[i]]
	end

	return new_order

end

def isclockwise(polygon)
	sum = 0

	for i in 0..polygon.length()-1 do
		j = (i+1) % polygon.length()
		sum += (polygon[j][0] - polygon[i][0]) * (polygon[j][1] + polygon[i][1])
	end

	if sum > 0
		return true
	else
		return false
	end
end
