# Ruby Script to create a procedural biome map in a Dungeondraft map file
# Version 1.00, Updated: 30-Jan-24, Author: u/uchideshi34

require 'json'
require 'optparse'
require_relative 'randombiome-maths'

$permutation_size = 256

## SECTION 1: functions to output data to flat files

def outputppm (filename, array, array_x, array_y,granularity)
	myfile = File.open(filename, "w")
	myfile.write("P2\n")
	myfile.write("# Grayscale version\n\n")
	myfile.write("#{array_x} #{array_y}\n")
	myfile.write("#{granularity}\n")
	for j in 0..array_x-1 do
		for i in 0..array_y-1 do
			myfile.write("#{array[i][j].to_s().rjust(4)}")
		end
		myfile.write("\n")
	end
	myfile.close
end

def store_perlin(array, outputfile)

    hash = Hash.new
    hash[:perlin] = array
    File.open(outputfile, "wb") { |file| file.puts JSON.pretty_generate(hash) }
  
  end
  
def retrieve_perlin(inputfile)
    hash = JSON.parse(File.read(inputfile))
    return hash["perlin"]
end

## SECTION 2: functions that take inputs and write them into the DD base_hash format
# Update the core hash to have the right map size
def update_core_map_hash()

    # Read node_id if it is non-zero
    $next_node_id = 0

    # Update map size
    $base_hash['world']['width'] = $config_hash["core"]["map_size"][0]
    $base_hash['world']['height'] = $config_hash["core"]["map_size"][1]

    $base_hash['header']['editor_state']['camera_position'] = "Vector2( #{$config_hash["core"]["map_size"][0]/2*256}, #{$config_hash["core"]["map_size"][1]/2*256} )"
    $base_hash['header']['editor_state']['camera_zoom'] = 8

    # Update Terrain
    str = " 255, 0, 0, 0," * ($config_hash["core"]["map_size"][0]*$config_hash["core"]["map_size"][1]*16)
    $base_hash['world']['levels']['0']['terrain']['splat'] = "PoolByteArray(#{str.chop})"

    # Update Tiles
    poolintarray = Array.new($config_hash["core"]["map_size"][0]*$config_hash["core"]["map_size"][1],-1)
    $base_hash['world']['levels']['0']['tiles']['cells'] = pistr(poolintarray)

    poolintarray = Array.new($config_hash["core"]["map_size"][0]*$config_hash["core"]["map_size"][1],"ffffffff")
    $base_hash['world']['levels']['0']['tiles']['colors'] = poolintarray

    # Update Cave arrays to have 2xy + 1.5x + 1.5y + 2 (round down) separate entries. No idea why this is.
    str = " 0," * ($config_hash["core"]["map_size"][0]*$config_hash["core"]["map_size"][1]*2 + 1.5 * ($config_hash["core"]["map_size"][0] + $config_hash["core"]["map_size"][1]) + 2).floor()
    $base_hash['world']['levels']['0']['cave']['bitmap'] = "PoolByteArray(#{str.chop})"
    $base_hash['world']['levels']['0']['cave']['entrance_bitmap'] = "PoolByteArray(#{str.chop})"

    if $config_hash.has_key?('trees')
        if $config_hash["trees"].has_key?('tree_type_list')

            for i in 0..$config_hash["trees"]["tree_type_list"].length()-1 do

                if $config_hash["trees"]["tree_type_list"][i].has_key?('canopy')

                    $base_hash['world']['levels']['1'] = JSON.parse(JSON.generate($base_hash['world']['levels']['0']))
                    $base_hash['world']['levels']['1']['terrain']['enabled'] = false
                    $base_hash['world']['levels']['1']['label'] = "Canopy"

                    break

                end

            end
        

        end

    end

end

def draw_object(x,y,rotation,size,texture,layer,mirror,custom_color_list,maplevel)
  object_hash = {"mirror": false}
  object_hash[:position] = "Vector2( #{x*256}, #{y*256} )"
  object_hash[:texture] = texture
  object_hash[:layer] = layer
  object_hash[:scale] = "Vector2( #{size}, #{size} )"
  object_hash[:rotation] = rotation
	object_hash[:mirror] = mirror
  object_hash[:shadow] = false
  if custom_color_list != nil
		object_hash[:custom_color] = custom_color_list.sample
	end
  object_hash[:node_id] = $next_node_id.to_s(16)
  $next_node_id += 1
  $base_hash['world']['levels'][maplevel]['objects'] << object_hash
end

def draw_terrain(output,xtraterrain,max_x_squares,max_y_squares)
	splat = Array.new
	splat2 = Array.new
	temp_array = Array.new(max_x_squares*4){Array.new(max_x_squares*4){Array.new(4,0)}}
	t = Time.now

	puts "Writing out terrain to hash..."
	if !xtraterrain
		#$base_hash['world']['levels']['0']['terrain']['splat'] = "PoolByteArray(#{output.flatten().join(", ")})"
		#return
		splat_str = "PoolByteArray( "
		for j in 0..max_y_squares*4-1 do
			for i in 0..max_x_squares*4-1 do

				for k in 0..3 do
					#splat_str = splat_str + "#{output[i][j][k]}, "
					temp_array[i][j][k] = output[j][i][k]
				end
			end
		end
		$base_hash['world']['levels']['0']['terrain']['splat'] = "PoolByteArray(#{temp_array.flatten().join(", ")})"
		
		#splat_str = splat_str.chop()
		#splat_str = splat_str.chop()
		#splat_str = splat_str + ")"
		#$base_hash['world']['levels']['0']['terrain']['splat'] = splat_str
		
		puts "Finished applying terrain to hash. Time taken #{(Time.now-t).floor(3)}s"
		return
	end
	for i in 0..max_x_squares*4-1 do
		for j in 0..max_y_squares*4-1 do
			splat += output[i][j][0..3]
			splat2 += output[i][j][4..7]
		end
	end
	puts "Made terrain arrays for splat and splat2. Time taken #{(Time.now-t).floor(3)}s."

	for i in 0..3 do
		if $config_hash["terrain"]["slots"][i]["overwrite_slot"]
			$base_hash['world']['levels']['0']['terrain']["texture_#{i+1}"]=$config_hash["terrain"]["slots"][i]["texture"]
		end
	end

	$base_hash['world']['levels']['0']['terrain']['splat'] = "PoolByteArray(#{splat.flatten().join(", ")})"
	$base_hash['world']['levels']['0']['terrain']['splat2'] = "PoolByteArray(#{splat2.flatten().join(", ")})"
	puts "Finished applying terrain to hash."
	return
end

def draw_light(x,y,light_colour,light_intensity,light_radius,maplevel)

	light_hash = Hash.new

	light_hash = {
		"position": "Vector2( 3444, 2976 )",
		"rotation": 0,
		"texture": "res://textures/lights/soft.png",
		"shadows": true
	}

	light_hash[:node_id] = $next_node_id.to_s(16)
	$next_node_id += 1
	light_hash[:range] = light_radius
	light_hash[:intensity] = light_intensity
	light_hash[:color] = light_colour
	light_hash[:position] = "Vector2( #{x*256}, #{y*256} )"

	$base_hash['world']['levels'][maplevel.to_s()]['lights'] << light_hash

end

def draw_pattern(polygonarray, layer, texture, colour_str, rotation)

	pattern_hash = Hash.new()
	vector_array = Array.new()

	pattern_hash = {
		"position": "Vector2( 0, 0 )",
		"shape_rotation": 0,
		"scale": "Vector2( 1, 1 )",
		"points": "PoolVector2Array( 384, 2048, 2176, 2048, 2176, 2944, 384, 2944 )",
		"outline": false
	}

	pattern_hash[:rotation] = (rotation / 360.0) * 2 * Math::PI
	pattern_hash[:color] = colour_str
	pattern_hash[:texture] = texture
	pattern_hash[:node_id] = $next_node_id.to_s(16)
	$next_node_id += 1
	pattern_hash[:layer] = layer

	for i in 0..polygonarray.length()-1 do
		vector_array << polygonarray[i]*256
	end

	pattern_hash[:points] = "PoolVector2Array( #{vector_array.join(", ")} )"

	$base_hash['world']['levels']['0']['patterns'] << pattern_hash

	return
end

def draw_water(waterpolygonarray,blend_distance,deep_color,shallow_color,parent_reference)

	vector_array = Array.new()
	water_hash = Hash.new()
	reference = 0

	water_instance_hash = {
		"join": 0,
		"end": 0,
		"is_open": false,
		"children": []
	}
	water_instance_hash[:ref] = rand(999999999)
	water_instance_hash[:blend_distance] = blend_distance
	water_instance_hash[:deep_color] = deep_color
	water_instance_hash[:shallow_color] = shallow_color
	for i in 0..waterpolygonarray.length()-1 do
		vector_array << waterpolygonarray[i]*256
	end
	water_instance_hash[:polygon] = "PoolVector2Array( #{vector_array.join(", ")} )"

	if !$base_hash['world']['levels']['0']['water'].has_key?(:tree)
		water_hash = {
			"disable_border": true,
			"tree": {
				"ref": 951149238,
				"polygon": "PoolVector2Array(  )",
				"join": 0,
				"end": 0,
				"is_open": false,
				"deep_color": "00000000",
				"shallow_color": "00000000",
				"blend_distance": 0,
				"children": []
				}
			}
		water_hash[:tree][:ref] = rand(999999999)
		if $config_hash.has_key?('swamp')
			if $config_hash["swamp"].has_key?('disable_border')
				if $config_hash["swamp"]["disable_border"] == false
					water_hash["disable_border"] = false
				end
			end
		end
	else
		water_hash = $base_hash['world']['levels']['0']['water']
	end

	if parent_reference == 0
		water_hash[:tree][:children] << water_instance_hash
	else
		found = 0
		for i in 0..water_hash[:tree][:children].length()-1 do
			if water_hash[:tree][:children][i][:ref] == parent_reference
				found = 1
				water_hash_parent = water_hash[:tree][:children][i]
			end
		end
		if found == 0
			puts "Didn't find parent reference"
		end
		water_hash_parent[:children] << water_instance_hash
	end


	$base_hash['world']['levels']['0']['water'] = water_hash

	return water_instance_hash[:ref]

end

def draw_path(vector_array, end_point_type, width, texture, layer, loop, maplevel, donotreverse)
    path = {
        "rotation": 0,
        "scale": "Vector2( 1, 1 )",
        "smoothness": 1,
        "layer": layer,
        "fade_in": false,
        "fade_out": false,
        "grow": false,
        "shrink": false,
        "loop": false,
    }
    
    path[:texture] = texture

	if vector_array.length() < 3
		return
	end

    if !donotreverse
        vector_array = vector_array.reverse()
    end

    path[:position] = "Vector2( #{(vector_array[0][0]*256).floor(1)}, #{(vector_array[0][1]*256).floor(1)} )"
    path[:width] = width
    path[:edit_points] = "PoolVector2Array( 0, 0"
    
    for i in 1..vector_array.length-1 do
        path[:edit_points] += ", #{((vector_array[i][0]-vector_array[0][0])*256).floor(1)}, #{((vector_array[i][1]-vector_array[0][1])*256).floor(1)}"
    end
    path[:edit_points] += " )"
    path[:node_id] = $next_node_id.to_s(16)
    if end_point_type == 0
        path[:fade_in] = true
        path[:fade_out] = true
    elsif end_point_type == 2
		path[:grow] = true
		path[:shrink] = true
    end
	path[:loop] = loop
    $base_hash['world']['levels'][maplevel]['paths'] << path
    $next_node_id += 1
end


## SECTION 3: utility functions that require config specific data or map context

def output_path(pathname,path)

    for i in 0..path.length()-1 do
        puts "#{[pathname]}[#{i}]: #{path[i][0]}, #{path[i][1]}"
    end

end

# Create a PoolInt string from an array
def pistr(array)
    str = "PoolIntArray("
    array.each do |value|
      str = "#{str}#{value},"
    end
    str = "#{str.chop} )"
    return str
end

def create_rotation(randomrotation,rotation_offset)

	if rotation_offset == nil
		rotation_offset = 0
	end

	if randomrotation
		rotation = rand*2.0*Math::PI - Math::PI
	elsif (rotation_offset.to_i()).abs() > 0
		rotation = ((rotation_offset+180) % 360 - 180) / 360.0 * 2.0 * Math::PI
	else
		rotation = 0.0
	end
	return rotation
end

def create_mirror(randommirror)
	if randommirror
		mirror = [true, false].sample
	else
		mirror = false
	end
	return mirror
end

# Compress perlin down to a 2d array with 16 points per map square
def simplifyperlin(perlin)
	puts "Simplifying Perlin map down to array with 16 points per square."
	t = Time.now

	output = Array.new($config_hash["core"]["map_size"][0]*4){Array.new($config_hash["core"]["map_size"][1]*4)}

	for i in 0..$config_hash["core"]["map_size"][0]*4-1 do
		for j in 0..$config_hash["core"]["map_size"][1]*4-1 do
			nearest_x = (i*$config_hash["perlin_noise"]["size"]/($config_hash["core"]["map_size"][0]*4)).floor()
			nearest_y = (j*$config_hash["perlin_noise"]["size"]/($config_hash["core"]["map_size"][1]*4)).floor()
			output[i][j] = perlin[nearest_x][nearest_y]
		end
	end

	puts "Finished simplying Perlin. Time taken: #{(Time.now-t).floor(3)}s.\n\n"
	return output

end

# Return a list of points in i,j from a path array in x,y
def findpointsarrayfrompath(patharray)

    pointsarray = Array.new(){Array.new(2)}
    delta = Array.new(2)
    point = Array.new(2)
    increments = 10

    pointsarray << [(patharray[0][0]*4).floor(),(patharray[0][1]*4.0).floor()]
    last_point = [(patharray[0][0]*4).floor(),(patharray[0][1]*4.0).floor()]

    for i in 0..patharray.length()-2 do

        j = (i + 1)

        delta[0] = (patharray[j][0] - patharray[i][0]).to_f()/increments
        delta[1] = (patharray[j][1] - patharray[i][1]).to_f()/increments

        for k in 1..increments-1 do

            point[0] = ((patharray[i][0] + k * delta[0])*4.0).floor()
            point[1] = ((patharray[i][1] + k * delta[1])*4.0).floor()

            if !(point[0] == last_point[0] && point[1] == last_point[1])
                pointsarray << [point[0],point[1]]
                last_point[0] = point[0]
                last_point[1] = point[1]
            end

        end

    end
    pointsarray << [(patharray[-1][0]*4).floor(),(patharray[-1][1]*4.0).floor()]

    #Flush points that can't be displayed
    original_length = pointsarray.length()
    i = 0
    for j in 0..original_length-1 do

        if pointsarray[i][0] < 0 || pointsarray[i][1] < 0 || pointsarray[i][0] > $config_hash["core"]["map_size"][0]*4 - 1 || pointsarray[i][1] > $config_hash["core"]["map_size"][0]*4 - 1
            
            pointsarray.delete_at(i)
        else
            
            i += 1
        end
    end

    return pointsarray

end

def isvalidindex(i,j)
    if i >= 0 && i < $config_hash["core"]["map_size"][0]*4 && j >= 0 && j < $config_hash["core"]["map_size"][1]*4
        return true
    else
        return false
    end
end

def setpoint(points_area,i,j,newvalue,new_array)

	if isvalidindex(i,j)
		if points_area[i][j] == -1
			points_area[i][j] = newvalue
			new_array << [i,j]
			return true
		elsif points_area[i][j] > newvalue
			points_area[i][j] = newvalue
			return true
		else
			return false
		end
	else
		return false
    end

end

def finddistancefrompath(current_point,original_path)

	min_dist = 99999.9
	min_index = -1
	for j in 0..original_path.length()-1 do
		distance = finddistancebetweenpoints(current_point,original_path[j])
			
		if distance < min_dist
			min_dist = distance
			min_index = j
		end

	end

	return min_dist
end

#Take an area with some values defined and write values decremented to the points on the left hand to the path
def growpointsarea(points_area,pointsarray)

    newvalue = 99999.9
	new_pointsarray = Array.new(){Array.new(2)}

    for i in 0..pointsarray.length()-1 do
        if isvalidindex(pointsarray[i][0],pointsarray[i][1])
            setpoint(points_area,pointsarray[i][0]-1,pointsarray[i][1],newvalue,new_pointsarray)
            setpoint(points_area,pointsarray[i][0]+1,pointsarray[i][1],newvalue,new_pointsarray)
            setpoint(points_area,pointsarray[i][0],pointsarray[i][1]-1,newvalue,new_pointsarray)
			setpoint(points_area,pointsarray[i][0],pointsarray[i][1]+1,newvalue,new_pointsarray)
        end
    end

	return new_pointsarray

end

def isedgecontour(contour)

	map_size = $config_hash["core"]["map_size"][0].floor()

	if contour[0][0] <= 0.0 || contour[0][1] <= 0.0 || contour[0][0] >= map_size || contour[0][1] >= map_size || contour[-1][0]  <= 0.0 || contour[-1][1]  <= 0.0 || contour[-1][0] >= map_size || contour[-1][1] >= map_size
		return true
	else
		return false
	end
end

def findcontours(case_array,filter)

  	contours = Array.new(){Array.new(){Array.new(2)}}
  	contour = Array.new(){Array.new(2)}
	min_contour_length = 5
	nomoreedges = false

  	for j in 0..2000 do
		contour = findcontour(case_array,filter)
		if contour == nil
			break
		end
		if isedgecontour(contour) == false && nomoreedges == false
			nomoreedges = true
		end
		if contour.length() > min_contour_length
			contours << contour
		end
	end

  return contours

end

def drawcontourpath(contour,texture,layer,width,reverse)

	new_path = Array.new(){Array.new(2)}

	isedge = isedgecontour(contour)

  	for i in 0..contour.length()-1 do
		new_path << [contour[i][0], contour[i][1]]
  	end

	if new_path.length()-1 > 0
		
		draw_path(new_path, 1, width, texture, layer, !isedge,'0', !reverse)

	end
end

def findnextvalidoffsetpoint(startoffsetindex,offsetpath,pathstartindex,path,offset_distance)

    #Look for the next offset path point that is not too close
    for k in 1..offsetpath.length()-1 do
        check_index = (startoffsetindex + k) % offsetpath.length()
        if finddistancebetweenpoints(offsetpath[check_index],path[pathstartindex]) < offset_distance.abs()*0.99
            #The next point is still too close so keep going
            next
        else
            isvalid = true
            for i in 0..path.length()-1 do
                check_path_index = (pathstartindex + i) % path.length()
                if finddistancebetweenpoints(offsetpath[check_index],path[check_path_index]) < offset_distance.abs()*0.99
                    isvalid = false
                    break
                end
            end
            
            if isvalid
                #If we have found a valid offset point then break the loop and return check_index
                break
            else
                #If we have not found a valid offset point then look at the next offset point
                next
            end
        end
    end

    if isvalid
        return check_index
    else
        return nil
    end

end


#Take a path and make all the distances between the points the same
def normalise_path(path)

	if path.length() < 3
		return []
	end

    output_path = Array.new(path.length()){Array.new(2)}
    distance_between_points = Array.new(path.length()-1){Array.new(2)}
    last_point = Array.new(2,0.0)
    pathlength = 0.0

	

    for i in 0..path.length()-2 do
        distance = finddistancebetweenpoints(path[i],path[i+1])
        pathlength += distance
        distance_between_points[i] = distance
    end

    new_distance = pathlength / (path.length()-1)

    previous_index = 0

    output_path[0] = [path[0][0],path[0][1]]
    distance_to_last_original_point = 0.0
    j = 0

    for i in 1..path.length()-2 do

        distance_remaining = new_distance
        
        for k in 0..path.length()-2 do

            if distance_remaining + distance_to_last_original_point < distance_between_points[j]
                
                output_path[i][0] = path[j][0] + (distance_remaining + distance_to_last_original_point)/distance_between_points[j] * (path[j+1][0]-path[j][0])
                output_path[i][1] = path[j][1] + (distance_remaining + distance_to_last_original_point)/distance_between_points[j] * (path[j+1][1]-path[j][1])
                distance_to_last_original_point += distance_remaining
                break
            else
                
                distance_remaining = distance_remaining + distance_to_last_original_point - distance_between_points[j]
                distance_to_last_original_point = 0.0
                j += 1

            end
            
            if j == path.length()-1
                break
            end

        end

    end

    output_path[-1] = [path[-1][0],path[-1][1]]

    return output_path

end

def make_offset_path(path,offset,strictdistance,islefthand)

	if path.length() < 3
		return []
	end

	if islefthand || strictdistance
		isclockwise = isclockwise(path)
	end

	if islefthand
		if isclockwise
			islefthandoffset = false
		else
			islefthandoffset = true
			offset = -offset
		end
	else
		islefthandoffset = false
	end

	if path == nil
		puts "Error in make_offset_path: Path is empty"
		exit
	end

	offsetpath = makeperpendicularpath(path,offset,islefthandoffset,nil)

    length = offsetpath.length()

    if strictdistance

		num_loops = 3
		loop_count = 0
        
		#Change the start conditions if this an edge path or a loop
		isedge = isedgecontour(path)
		if isedge
			validstart = true
			validstartindex = 0
			i = 2
		else
			validstart = false
			validstartindex = -1
			i = 0
			startcount = 0
		end
        actioned = false
        for n in 0..length*2 do
            
            isvalid = true
            
            for j in 0..path.length()-1 do
                #If the point in the offset is too close
                if finddistancebetweenpoints(offsetpath[i],path[j]) < offset.abs()*0.99

                    if !validstart
                        isvalid = false
                        break
                    end

                    #Look for the next offset path point that is not too close
                    start_index = i
                    check_index = findnextvalidoffsetpoint(i,offsetpath,j,path,offset.abs())

                    #Draw a bezier curve to the next valid point hoping this isn't too far away
                    curve_start = offsetpath[(start_index-2) % offsetpath.length()]
                    curve_middle =  offsetpath[(start_index-1) % offsetpath.length()]
                    curve_end = offsetpath[check_index]
                    bez = Bezier::Curve.new(curve_start, curve_middle, curve_end)

                    #Define points for all the invalid offset points
                    num_points = (check_index - start_index) % offsetpath.length()
                    for k in 0..num_points-1 do
                        point_on_curve_value = (k.to_f()*0.5/num_points) + 0.5
                        index = (start_index + k) % offsetpath.length()
                        offsetpath[index] = [bez.point_on_curve(point_on_curve_value).x,bez.point_on_curve(point_on_curve_value).y]
                    end

                    i = check_index
                    actioned = true
                    break

                end

            end

            i = (i + 1) % offsetpath.length()


            #If we have looped to the beginning we know that the first two points are okay so break there
            if validstart
				if i == validstartindex || check_index == validstartindex
					loop_count += 1
					if loop_count >= num_loops || !actioned
						break
					else
						actioned = false
					end
                end
            end
			
			#If this isn't a looping path, we want to always skip the first two points and assume they are valid
			if isedge
				if i < 2
					i = 2
				end
			end

            #See if we have two subsequent valid points as this is required to start the algorithm
            if isvalid && !validstart
                startcount += 1
                if startcount == 2
                    validstart = true
                    validstartindex = (i - 2) % offsetpath.length() 
                end
            elsif !isvalid && !validstart
                startcount = 0
            end
            
        end
        offsetpath = normalise_path(offsetpath)
    end

    return offsetpath
end

## SECTION 4: terrain creation related functions

# Returns a 3d array of the terrain values, this function was intended to manage using additional terrain slots but it somewhat redundant now.
def make_terrain_array(splat_str, splat2_str,max_x_squares,max_y_squares)

	t = Time.now

	puts "Creating terrain array..."

	output = Array.new(max_x_squares*4){Array.new(max_y_squares*4){Array.new(4,0)}}

    puts "Terrain array creation complete... Time taken #{(Time.now-t).floor(3)}s.\n\n"
	return output
end

def apply_perlin_to_terrain(simple_perlin, terrain)
	puts "Applying Perlin to terrain..."

    granularity = $config_hash["perlin_noise"]["granularity"]

    #Define 4 areas where each of the respective terrain types dominate, the 3 boundaries define the internal edges of those areas, eg 255 to boundary[0] is the area where terrain[0] dominates.
    boundaries = Array.new(3)
    #Define 4 weights for each of the respective terrain types to multiply the amount of dominance
    weights = Array.new(4)
    #Define 3 transition distances between each area, ie blend_distance[0] is between area[0] and area[1]
    blend_distances = Array.new(3)

    #Define 7 sections where different formulas apply
    sections = Array.new(6)

    #Define 2 formula outputs for each terrain type
    calc_one = Array.new(4)
    calc_two = Array.new(4)

    check_terrain = Array.new(4){Array.new(simple_perlin.length()){Array.new(simple_perlin.length())}}
    default = Array.new(4)
	t = Time.now

    boundaries[0] = $config_hash["terrain"]["boundaries"][0]
    boundaries[1] = $config_hash["terrain"]["boundaries"][1]
    boundaries[2] = $config_hash["terrain"]["boundaries"][2]

    weights[0] = $config_hash["terrain"]["weights"][0]
    weights[1] = $config_hash["terrain"]["weights"][1]
    weights[2] = $config_hash["terrain"]["weights"][2]
    weights[3] = $config_hash["terrain"]["weights"][3]

    blend_distances[0] = $config_hash["terrain"]["blend_distances"][0]
    blend_distances[1] = $config_hash["terrain"]["blend_distances"][1]
    blend_distances[2] = $config_hash["terrain"]["blend_distances"][2]


    for i in 0..2 do
        sections[i*2] = boundaries[i]+(blend_distances[i]*0.5).floor()
        sections[i*2+1] = boundaries[i]-(blend_distances[i]*0.5).floor()

    end

	for j in 0..simple_perlin.length()-1 do
		for i in 0..simple_perlin.length()-1 do
			#Find nearest Perlin point
			value = 255 - (simple_perlin[i][j] * 255/granularity).to_i()

			
			terrain[i][j][0] = 0
			terrain[i][j][1] = 0
            terrain[i][j][2] = 0
			terrain[i][j][3] = 0

            base = 255

            case value
			when sections[0]..255
				calc_one[0] = [(base * weights[0]).floor(),255].min
                calc_one[1] = 255 - calc_one[0]
                calc_one[2] = 0
                calc_one[3] = 0
                terrain[i][j][0] = calc_one[0]
			    terrain[i][j][1] = calc_one[1]
                terrain[i][j][2] = calc_one[2]
			    terrain[i][j][3] = calc_one[3]
                

            when sections[1]..sections[0]
                calc_one[0] = [(base * weights[0]).floor(),255].min
                calc_one[1] = 255 - calc_one[0]
                calc_one[2] = 0
                calc_one[3] = 0
                calc_two[1] = [(base * weights[1]).floor(),255].min
                calc_two[0] = ((255 - calc_two[1])*0.5).floor()
                calc_two[2] = 255 - calc_two[0] - calc_two[1]
                calc_two[3] = 0
                terrain[i][j][0] = ((calc_one[0] - calc_two[0]).to_f() * (value - sections[1]).to_f() / (sections[0]-sections[1]).to_f()).floor() + calc_two[0]
			    terrain[i][j][1] = ((calc_one[1] - calc_two[1]).to_f() * (value - sections[1]).to_f() / (sections[0]-sections[1]).to_f()).floor() + calc_two[1]
                terrain[i][j][2] = 255 - terrain[i][j][0] - terrain[i][j][1]
			    terrain[i][j][3] = ((calc_one[3] - calc_two[3]).to_f() * (value - sections[1]).to_f() / (sections[0]-sections[1]).to_f()).floor() + calc_two[3]
            when sections[2]..sections[1]
                calc_one[1] = [(base * weights[1]).floor(),255].min
                calc_one[0] = ((255 - calc_one[1])*0.5).floor()
                calc_one[2] = 255 - calc_one[0] - calc_one[1]
                calc_one[3] = 0
                terrain[i][j][0] = calc_one[0]
			    terrain[i][j][1] = calc_one[1]
                terrain[i][j][2] = calc_one[2]
			    terrain[i][j][3] = calc_one[3]
            when sections[3]..sections[2]
                calc_one[1] = [(base * weights[1]).floor(),255].min
                calc_one[0] = ((255 - calc_one[1])*0.5).floor()
                calc_one[2] = 255 - calc_one[0] - calc_one[1]
                calc_one[3] = 0
                calc_two[2] = [(base * weights[2]).floor(),255].min
                calc_two[0] = 0
                calc_two[1] = ((255 - calc_two[2])*0.5).floor()
                calc_two[3] = 255 - calc_two[2] - calc_two[1]
                terrain[i][j][0] = ((calc_one[0] - calc_two[0]).to_f() * (value - sections[3]).to_f() / (sections[2]-sections[3]).to_f()).floor() + calc_two[0]
			    terrain[i][j][1] = ((calc_one[1] - calc_two[1]).to_f() * (value - sections[3]).to_f() / (sections[2]-sections[3]).to_f()).floor() + calc_two[1]
                terrain[i][j][2] = ((calc_one[2] - calc_two[2]).to_f() * (value - sections[3]).to_f() / (sections[2]-sections[3]).to_f()).floor() + calc_two[2]
			    terrain[i][j][3] = 255 - terrain[i][j][0] - terrain[i][j][1] - terrain[i][j][2]
                
            when sections[4]..sections[3]
                calc_one[2] = [(base * weights[2]).floor(),255].min
                calc_one[0] = 0
                calc_one[1] = ((255 - calc_one[2])*0.5).floor()
                calc_one[3] = 255 - calc_one[2] - calc_one[1]
                terrain[i][j][0] = calc_one[0]
			    terrain[i][j][1] = calc_one[1]
                terrain[i][j][2] = calc_one[2]
			    terrain[i][j][3] = calc_one[3]
            when sections[5]..sections[4]
                calc_one[2] = [(base * weights[2]).floor(),255].min
                calc_one[0] = 0
                calc_one[1] = ((255 - calc_one[2])*weights[1]/(weights[1]+weights[3])).floor()
                calc_one[3] = 255 - calc_one[2] - calc_one[1]
                calc_two[3] = [(base * weights[3]).floor(),255].min
                calc_two[0] = 0
                calc_two[1] = ((255 - calc_two[3])*weights[1]/(weights[1]+weights[2])).floor()
                calc_two[2] = 255 - calc_two[3] - calc_two[1]
                
                
			    terrain[i][j][1] = ((calc_one[1] - calc_two[1]).to_f() * (value - sections[5]).to_f() / (sections[4]-sections[5]).to_f()).floor() + calc_two[1]
                terrain[i][j][2] = ((calc_one[2] - calc_two[2]).to_f() * (value - sections[5]).to_f() / (sections[4]-sections[5]).to_f()).floor() + calc_two[2]
			    terrain[i][j][3] = ((calc_one[3] - calc_two[3]).to_f() * (value - sections[5]).to_f() / (sections[4]-sections[5]).to_f()).floor() + calc_two[3]
                terrain[i][j][0] = 255 - terrain[i][j][3] - terrain[i][j][1] - terrain[i][j][2]
                
            when 0..sections[5]
                calc_one[3] = [(base * weights[3]).floor(),255].min
                calc_one[0] = 0
                calc_one[1] = ((255 - calc_one[3])*0.5).floor()
                calc_one[2] = 255 - calc_one[3] - calc_one[1]
                
                terrain[i][j][0] = calc_one[0]
			    terrain[i][j][1] = calc_one[1]
                terrain[i][j][2] = calc_one[2]
			    terrain[i][j][3] = calc_one[3]
                
            end


            #puts "value: #{value}\t terrain[#{i}][#{j}]: #{terrain[i][j]}\t calc_one: #{calc_one}\t calc_two: #{calc_two}"

            check_terrain[0][i][j] = terrain[i][j][0]
            check_terrain[1][i][j] = terrain[i][j][1]
            check_terrain[2][i][j] = terrain[i][j][2]
            check_terrain[3][i][j] = terrain[i][j][3]

            next


            terrain[i][j][2] = 255 - value
			case value

			when high..255
				terrain[i][j][0] = [(value*highest_weight).to_i(),255].min
				terrain[i][j][1] = [255 - terrain[i][j][2] - terrain[i][j][0],0].max
				terrain[i][j][3] = 0
            when high-flex..high
				terrain[i][j][0] = [(value*highest_weight*high_flex_factor).to_i(),255].min
				terrain[i][j][1] = 255 - terrain[i][j][2] - terrain[i][j][0]
				terrain[i][j][3] = 0
			when 0..low
				terrain[i][j][3] = [((255 - value)*low_weight).floor(),255].min
				terrain[i][j][2] = 255 - terrain[i][j][3]
			when low..low+flex
				terrain[i][j][3] = [((255 - value)*low_weight*low_flex_factor).floor(),255].min
				terrain[i][j][2] = 255 - terrain[i][j][3]
			else
				terrain[i][j][2] = [((255-value)*open_area_weight).to_i(),255].min()
				terrain[i][j][1] = 255 - terrain[i][j][2]
			end






		end
	end
    for i in 0..3 do

        #outputppm("check_terrain#{i}.ppm",check_terrain[i],check_terrain[i].length(),check_terrain[i].length(),256)
    end
	puts "Perlin successfully applied to terrain. Time taken #{(Time.now-t).floor(3)}s.\n\n"

end

def apply_lighting(simple_perlin_map)

	if !$config_hash.has_key?("lighting")
		return
	end
	if !$config_hash["lighting"]["draw_lights"]
		return
	end

	puts "Applying lighting effects."
	t = Time.now()

	$base_hash['world']['levels']['0']['environment']['ambient_light'] = $config_hash["lighting"]["ambient_light"]
	if $base_hash['world']['levels'].has_key?('1')
		$base_hash['world']['levels']['1']['environment']['ambient_light'] = $config_hash["lighting"]["ambient_light"]
	end


	viewport = [0,$config_hash["core"]["map_size"][0],0,$config_hash["core"]["map_size"][1]]
	light_min_distance = 1/$config_hash["lighting"]["primary_light"]["light_density"]

	if $config_hash["lighting"]["primary_light"].has_key?('edge_min_factor')
		edge_min = $config_hash["lighting"]["primary_light"]["light_radius"] * $config_hash["lighting"]["primary_light"]["edge_min_factor"]
	else
		edge_min = $config_hash["lighting"]["primary_light"]["light_radius"] * 0.15
	end

	poissonDisk = PoissonDisk.new(viewport, light_min_distance, $config_hash["poissondisk"]["max_tries"])
	points = poissonDisk.all()

	for i in 0..points.length()-1 do

		x = points[i][:x]
		y = points[i][:y]

		distance_from_middle = Math.sqrt((x-$config_hash["core"]["map_size"][0]/2.0)**2 + (y-$config_hash["core"]["map_size"][1]/2.0)**2)

		if x < edge_min || y < edge_min || x > $config_hash["core"]["map_size"][0] - edge_min || y > $config_hash["core"]["map_size"][1] - edge_min
			is_close_to_edge = true
		else
			is_close_to_edge = false
		end

		if distance_from_middle < $config_hash["core"]["map_size"][0]*0.5 + $config_hash["lighting"]["primary_light"]["light_radius"] * Math.sqrt(2) && !is_close_to_edge

			draw_light(x,y,$config_hash["lighting"]["primary_light"]["light_colour"],$config_hash["lighting"]["primary_light"]["light_intensity"],$config_hash["lighting"]["primary_light"]["light_radius"],'0')
			if $base_hash['world']['levels'].has_key?('1')
				draw_light(x,y,$config_hash["lighting"]["primary_light"]["light_colour"],$config_hash["lighting"]["primary_light"]["light_intensity"],$config_hash["lighting"]["primary_light"]["light_radius"],'1')
			end
		end

	end

	if $config_hash["lighting"].has_key?("perlin_level_lights")
		for i in 0..$config_hash["lighting"]["perlin_level_lights"].length()-1 do
			scatter_lights_by_level(simple_perlin_map,$config_hash["lighting"]["perlin_level_lights"][i]["upper_perlin_level"],
						$config_hash["lighting"]["perlin_level_lights"][i]["lower_perlin_level"],$config_hash["lighting"]["perlin_level_lights"][i]["light_colour"],
						$config_hash["lighting"]["perlin_level_lights"][i]["light_radius"],
						$config_hash["lighting"]["perlin_level_lights"][i]["light_density"],$config_hash["lighting"]["perlin_level_lights"][i]["light_intensity"],
						$config_hash["lighting"]["perlin_level_lights"][i]["probability"])
		end

	end
	puts "Lighting complete. Time taken: #{(Time.now()-t).floor(2)}s.\n\n"

end

def scatter_lights_by_level(simple_perlin_map,upper_perlin_level,lower_perlin_level,light_colour,light_radius,light_density,light_intensity,probability)

	viewport = [0,$config_hash["core"]["map_size"][0],0,$config_hash["core"]["map_size"][1]]
	light_min_distance = 1.0/light_density

	poissonDisk = PoissonDisk.new(viewport, light_min_distance, $config_hash["poissondisk"]["max_tries"])
	points = poissonDisk.all()

	for i in 0..points.length()-1 do

		pixelmatch_x = ((points[i][:x]*4).floor())
		pixelmatch_y = ((points[i][:y]*4).floor())

		if simple_perlin_map[pixelmatch_x][pixelmatch_y] < (upper_perlin_level * $config_hash["perlin_noise"]["granularity"])	&& simple_perlin_map[pixelmatch_x][pixelmatch_y] > (lower_perlin_level*$config_hash["perlin_noise"]["granularity"]) && rand < probability then
			draw_light(points[i][:x],points[i][:y],light_colour,light_intensity,light_radius,'0')
			if $base_hash['world']['levels'].has_key?('1')
				draw_light(points[i][:x],points[i][:y],light_colour,light_intensity,light_radius,'1')
			end
		end

	end

end

def make_terrain(simple_perlin_map)
	terrain = make_terrain_array($base_hash['world']['levels']['0']['terrain']['splat'],"",$config_hash["core"]["map_size"][0],$config_hash["core"]["map_size"][1])

	for i in 0..3 do
		if $config_hash["terrain"]["slots"][i]["overwrite_slot"]
			$base_hash['world']['levels']['0']['terrain']['texture_'+(i+1).to_s()] = $config_hash["terrain"]["slots"][i]["texture"]
		end
	end
	apply_perlin_to_terrain(simple_perlin_map, terrain)

	if $config_hash["terrain"].has_key?("smooth_blending")
		if $config_hash["terrain"]["smooth_blending"]
			$base_hash['world']['levels']['0']['terrain'][:smooth_blending] = true
		end
	end

    return terrain
end

## SECTION 5: object creation and distribution related functions

#Draw a clump at point x,y
def draw_clump(x,y,clump_hash,index)
	rotation = 0.0
	mirror = false

	num_assets = rand(clump_hash["primary_asset_list"][index]["num_secondary_assets_range"][0]..clump_hash["primary_asset_list"][index]["num_secondary_assets_range"][1])

	#Draw the secondary assets
	for i in 0..num_assets-1 do
		direction = rand(i.to_f()/num_assets..(i+1).to_f()/num_assets) * 2 * Math::PI - Math::PI

		distance = rand(2*clump_hash["primary_asset_list"][index]["secondary_assets_dist_variation_px"]) + clump_hash["primary_asset_list"][index]["secondary_assets_dist_px"] - clump_hash["primary_asset_list"][index]["secondary_assets_dist_variation_px"]
		distance = distance / 256.0

		out_x = Math.cos(direction) * distance + x
		out_y = Math.sin(direction) * distance + y

		texture = clump_hash["secondary_asset_list"].sample

		rotation = create_rotation(clump_hash["secondary_randomrotation"],0.0)
		mirror = create_mirror(clump_hash["primary_asset_list"][index]["secondary_randommirror"])
		if clump_hash["primary_asset_list"][index].has_key?('secondary_size_factor')
			size = clump_hash["primary_asset_list"][index]["secondary_size_factor"]
		else
			size = 1.0
		end

		draw_object(out_x,out_y,rotation,size,texture,clump_hash["secondary_asset_layer"],mirror,clump_hash["secondary_asset_colour_list"],'0')

	end

	#Draw the primary asset
	rotation = create_rotation(clump_hash["primary_asset_list"][index]["randomrotation"],clump_hash["primary_asset_list"][index]["rotation_offset"])
	mirror = create_mirror(clump_hash["primary_asset_list"][index]["randommirror"])

	draw_object(x,y,rotation,1.0,clump_hash["primary_asset_list"][index]["texture"],clump_hash["layer"],mirror,clump_hash["primary_asset_colour_list"],'0')

end

# Determine a location for a clump and then draw it there
def scatter_clump(pixels,clump_hash)

	viewport = [0,$config_hash["core"]["map_size"][0],0,$config_hash["core"]["map_size"][1]]
	poissonDisk = PoissonDisk.new(viewport, clump_hash["mindistance"], $config_hash["poissondisk"]["max_tries"])

	points = poissonDisk.all()
	list = Array.new

	#Make a list of indices to the primary_asset_list
	for i in 0..clump_hash["primary_asset_list"].length()-1
		list[i] = i
	end

	#Copy that list
	primary_list = list.map(&:clone)

	for i in 0..points.length-1 do
			pixelmatch_x = ((points[i][:x]*$config_hash["perlin_noise"]["size"]/$config_hash["core"]["map_size"][0]).floor())
			pixelmatch_y = ((points[i][:y]*$config_hash["perlin_noise"]["size"]/$config_hash["core"]["map_size"][1]).floor())

			if pixels[pixelmatch_x][pixelmatch_y] < clump_hash["upper_perlin_level"]*$config_hash["perlin_noise"]["granularity"] && pixels[pixelmatch_x][pixelmatch_y] > clump_hash["lower_perlin_level"]*$config_hash["perlin_noise"]["granularity"] && rand < clump_hash["probability"] then

				if primary_list.length()<2 then
					primary_list = list.map(&:clone)
				end
				index = rand(primary_list.length())

				draw_clump(points[i][:x],points[i][:y],clump_hash,primary_list[index])

				primary_list.delete_at(index)

			end
	end



end

def scatter_clumps(pixels)

	if !$config_hash.has_key?("clumps")
		return
	end

	puts "Drawing clumps of objects."
	t = Time.now()

	for i in 0..$config_hash["clumps"].length()-1 do
		scatter_clump(pixels,$config_hash["clumps"][i])
	end

	puts "Finished drawing clumps. Time taken: #{(Time.now()-t).floor(2)}s.\n\n"

end

def scatter_object_layer(pixels,mindistance,probability,upper_perlin_level,lower_perlin_level,randomrotation,size,layer,list,randommirror,rotation_offset,custom_color_list,maplevel)

	change_dimensions = false

	print "Scattering objects. "
  $stdout.flush

	viewport = [0,$config_hash["core"]["map_size"][0],0,$config_hash["core"]["map_size"][1]]
	if mindistance < 1.0
		viewport = [0,($config_hash["core"]["map_size"][0]/mindistance).floor(),0,($config_hash["core"]["map_size"][1]/mindistance).floor()]
		poissonDisk = PoissonDisk.new(viewport, 1.0, $config_hash["poissondisk"]["max_tries"])
		change_dimensions = true
	else
		viewport = [0,$config_hash["core"]["map_size"][0],0,$config_hash["core"]["map_size"][1]]
		poissonDisk = PoissonDisk.new(viewport, mindistance, $config_hash["poissondisk"]["max_tries"])
	end

	t = Time.now()
	points = poissonDisk.all()

	puts "Time taken: #{(Time.now()-t).floor(3)}s.\n"
	object_list = Array.new
	object_list = list.map(&:clone)

	for i in 0..points.length-1 do

		if change_dimensions
			x = points[i][:x]*mindistance
			y = points[i][:y]*mindistance
		else
			x = points[i][:x]
			y = points[i][:y]
		end
		pixelmatch_x = ((x * $config_hash["perlin_noise"]["size"]/$config_hash["core"]["map_size"][0]).floor())
		pixelmatch_y = ((y * $config_hash["perlin_noise"]["size"]/$config_hash["core"]["map_size"][1]).floor())

		if pixels[pixelmatch_x][pixelmatch_y] < upper_perlin_level*$config_hash["perlin_noise"]["granularity"] && pixels[pixelmatch_x][pixelmatch_y] > lower_perlin_level*$config_hash["perlin_noise"]["granularity"] && rand < probability then
			rotation = create_rotation(randomrotation,rotation_offset)
			mirror = create_mirror(randommirror)
			if object_list.length()<2 then
				object_list = list.map(&:clone)
			end
			index = rand(object_list.length())
			draw_object(x,y,rotation,rand*(size[1]-size[0])+size[0],object_list[index],layer,mirror,custom_color_list,maplevel)

			object_list.delete_at(index)

		end
	end

end

def scatter_trees(pixels)

	puts "Scattering trees..."
	tree_type = Hash.new

	for i in 0..$config_hash["trees"]["tree_type_list"].length()-1 do
		tree_type = $config_hash["trees"]["tree_type_list"][i]
		if $config_hash["trees"]["tree_type_list"][i].has_key?('randommirror')
			randommirror = $config_hash["trees"]["tree_type_list"][i]["randommirror"]
		else
			randommirror = false
		end
        if $config_hash["trees"]["tree_type_list"][i].has_key?('canopy')
            maplevel = "1"
        else
            maplevel = "0"
        end
		scatter_object_layer(pixels,tree_type["mindistance"],tree_type["probability"],tree_type["upper_perlin_level"],tree_type["lower_perlin_level"],tree_type["randomrotation"],tree_type["size"],tree_type["layer"],tree_type["list"],randommirror,tree_type["rotation_offset"],tree_type["custom_colour_list"],maplevel)

	end
	puts "Scatter trees complete."

end

def draw_shadow_object_for_tree(obj_index, tree_index)

    if $config_hash["trees"]["tree_type_list"][tree_index].has_key?("canopy")
        maplevel = "1"
    else
        maplevel = "0"
    end

	obj_location = $base_hash['world']['levels'][maplevel]['objects'][obj_index][:position][9..$base_hash['world']['levels']['0']['objects'][obj_index][:position].length()-2].split(", ")
	x = obj_location[0].to_f()/256.0
	y = obj_location[1].to_f()/256.0

	if $config_hash["trees"]["tree_type_list"][tree_index].has_key?('rotation_offset')
		rotation = ($config_hash["trees"]["tree_type_list"][tree_index]["rotation_offset"] + $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["rotation_offset"]).floor() % 360
	else
		rotation = $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["rotation_offset"].floor() % 360
	end

	size = $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["size"]
	texture = $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["texture_list"].sample
	layer = $config_hash["trees"]["tree_type_list"][tree_index]["layer"]
	mirror = create_mirror($config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["randommirror"])
   

 	if $config_hash["trees"]["tree_type_list"][tree_index]["shadows"].has_key?("centre_offset_dist")
 		distance = $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["centre_offset_dist"]
		angle = ((rotation + $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["centre_offset_angle"].floor()) % 360) / 360.0 * 2 * Math::PI - Math::PI
		x += - Math.sin(angle) * distance
		y += Math.cos(angle) * distance
	end

	rotation = rotation / 360.0 * 2 * Math::PI - Math::PI

	draw_object(x,y,rotation,size,texture,layer,mirror,["ff000000"],maplevel)

	if $config_hash["trees"]["tree_type_list"][tree_index]["shadows"]["place_under"]
		$base_hash['world']['levels'][maplevel]['objects'][-1][:node_id] = $base_hash['world']['levels'][maplevel]['objects'][obj_index][:node_id]
		$base_hash['world']['levels'][maplevel]['objects'][obj_index][:node_id] = $next_node_id
		$base_hash['world']['levels'][maplevel]['objects'][-1], $base_hash['world']['levels'][maplevel]['objects'][obj_index] = $base_hash['world']['levels'][maplevel]['objects'][obj_index], $base_hash['world']['levels'][maplevel]['objects'][-1]
	end


end

def apply_shadows_to_trees()
	obj_location = Array.new(2)
	obj_index = Array.new(2)
	object_array = Array.new(){Array.new(2)}
	store_delete_index = Array.new()
	reverse_direction = false
	t = Time.now()
	puts "Apply shadows to trees..."

    for n in 0..1 do
        if !$base_hash['world']['levels'].has_key?(n.to_s())
            next
        end

        for i in 0..$base_hash['world']['levels'][n.to_s()]['objects'].length()-1 do

            obj_location = $base_hash['world']['levels'][n.to_s()]['objects'][i][:position][9..$base_hash['world']['levels'][n.to_s()]['objects'][i][:position].length()-2].split(", ")
            texture = $base_hash['world']['levels'][n.to_s()]['objects'][i][:texture]


            for j in 0..$config_hash["trees"]["tree_type_list"].length()-1 do
                if $config_hash["trees"]["tree_type_list"][j].has_key?("canopy")
                    maplevel = "1"
                else
                    maplevel = "0"
                end
                if $config_hash["trees"]["tree_type_list"][j].has_key?("shadows")
                    if $config_hash["trees"]["tree_type_list"][j]["list"].include?(texture)
                        if $config_hash["trees"]["tree_type_list"][j]["shadows"]["make_shadow"]
                            if $config_hash["trees"]["tree_type_list"][j]["shadows"]["type"] == "path"

								if $config_hash["trees"]["tree_type_list"][j]["shadows"].has_key?("reverse_direction")
									if $config_hash["trees"]["tree_type_list"][j]["shadows"]["reverse_direction"]
										reverse_direction = true
									end
								end

                                shadow_array = make_circle($config_hash["trees"]["tree_type_list"][j]["shadows"]["asset_radius"]/256.0, [obj_location[0].to_f()/256.0,obj_location[1].to_f()/256.0], 25)
                                draw_path(shadow_array, 1, $config_hash["trees"]["tree_type_list"][j]["shadows"]["size"], $config_hash["trees"]["tree_type_list"][j]["shadows"]["texture"], $config_hash["trees"]["tree_type_list"][j]["layer"],true,maplevel,!reverse_direction)

                            end
                            if $config_hash["trees"]["tree_type_list"][j]["shadows"]["type"] == "object"

                                draw_shadow_object_for_tree(i,j)

                            end

                        end

                    end
                end
                if $config_hash["trees"]["tree_type_list"][j].has_key?("lights")
                    if $config_hash["trees"]["tree_type_list"][j]["list"].include?(texture)
                        if $config_hash["trees"]["tree_type_list"][j]["lights"]["draw_lights"] && $config_hash["lighting"]["draw_lights"]
                            
                            draw_light(obj_location[0].to_f()/256.0,obj_location[1].to_f()/256.0,$config_hash["trees"]["tree_type_list"][j]["lights"]["light_colour"],$config_hash["trees"]["tree_type_list"][j]["lights"]["light_intensity"],$config_hash["trees"]["tree_type_list"][j]["lights"]["light_radius"],maplevel)
                        end
                    end
                end
                if $config_hash["trees"]["tree_type_list"][j].has_key?("canopy")
                    if $config_hash["trees"]["tree_type_list"][j]["list"].include?(texture)
                        for k in 0..$config_hash["trees"]["tree_type_list"][j]["canopy"]["ground_asset_list"].length()-1 do

                            ground_object_hash = $config_hash["trees"]["tree_type_list"][j]["canopy"]["ground_asset_list"][k]
                            if ground_object_hash["randomrotation"]
                                rotation = rand() * 2 * Math::PI - Math::PI
                            else
                                rotation = 0.0
                            end
                            size = rand(ground_object_hash["size"][0]..ground_object_hash["size"][1])
                            if ground_object_hash["randomrotation"]
                                mirror = [true,false].sample
                            else
                                mirror = false
                            end

                            draw_object(obj_location[0].to_f()/256.0,obj_location[1].to_f()/256.0,rotation,size,ground_object_hash["texture_list"].sample,ground_object_hash["layer"],mirror,nil,'0')
                        end
                    end
                end
            end
        end
    end

	puts "Completed applying shadows to trees. Time taken: #{(Time.now()-t).floor(2)}s.\n\n"
end

def scatter_objects(pixels)
	puts "Scattering ground clutter..."
	t = Time.now()

	for i in 0..$config_hash["clutter"].length()-1 do
		object_type = $config_hash["clutter"][i]
		scatter_object_layer(pixels,object_type["mindistance"],object_type["probability"],object_type["upper_perlin_level"],object_type["lower_perlin_level"],object_type["randomrotation"],object_type["size"],object_type["layer"],object_type["list"],object_type["randommirror"],0,object_type["custom_colour_list"],'0')
	end
	puts "Scatter ground clutter complete. Time taken: #{(Time.now()-t).floor(2)}s.\n\n"

end

## SECTION 6: road definition related functions

def getsteplocations(maparray,start,direction,fov_angle,step_distance,stepoptions)

		stepoptions.clear()

		num_options = (fov_angle * 2.0 * step_distance * 4.5).ceil()
		
		for i in 0..num_options-1 do
			theta = (2.0*fov_angle/num_options*i - fov_angle + direction)
			x = -(Math.sin(theta) * step_distance*4).round()
			y = (Math.cos(theta) * step_distance*4).round()
			
            if start[0]+x < 0 || start[0]+x > $config_hash["core"]["map_size"][0]*4-1 || start[1]+y < 0 || start[1]+y > $config_hash["core"]["map_size"][1]*4-1
				next
			end
			stepoptions << [start[0]+x,start[1]+y]
			
		end
		return stepoptions.uniq()
end

def findbeststepoption(maparray,stepoptions,current_location,iseastwest,target_road_value)

	best_value = 0
	best_point = [0,0]
	weight = $config_hash["road"]["progress_weight"]
	margin = ($config_hash["road"]["margin_percentage"]*$config_hash["core"]["map_size"][1]*2).floor()

	for i in 0..stepoptions.length()-1 do

        if iseastwest
		    onwards_weight = 0.25*(stepoptions[i][0]-current_location[0])/$config_hash["road"]["step_distance"]*weight
        else
            onwards_weight = 0.25*(stepoptions[i][1]-current_location[1])/$config_hash["road"]["step_distance"]*weight
        end

		test_value = (maparray[stepoptions[i][0]][stepoptions[i][1]].to_f()/$config_hash["perlin_noise"]["granularity"] - target_road_value).abs() + onwards_weight

        if iseastwest
		    if stepoptions[i][1] < margin+$config_hash["road"]["width"]*2 || stepoptions[i][1] > $config_hash["core"]["map_size"][1]*4-margin-$config_hash["road"]["width"]*2
                test_value = test_value * 0.25
            end
        else
            if stepoptions[i][0] < margin+$config_hash["road"]["width"]*2 || stepoptions[i][0] > $config_hash["core"]["map_size"][1]*4-margin-$config_hash["road"]["width"]*2
                test_value = test_value * 0.25
            end
        end
		
		
    	if best_value < test_value
			best_value = test_value
			best_point = [stepoptions[i][0],stepoptions[i][1]]
		end
	end

	return best_point

end

def findwalkstart(simple_map)

	highest = 0
	index = 0
	margin = ($config_hash["road"]["margin_percentage"]*$config_hash["core"]["map_size"][1]*4).floor()+$config_hash["road"]["width"]*4

	for i in margin..$config_hash["core"]["map_size"][1]*4-margin-1 do
		if highest < simple_map[0][i]
			highest = simple_map[0][i]
			index = i
		end
		#puts "highest: #{highest} simple_map: #{simple_map[0][i]}, i: 0, j: #{i}"
	end

	return [index,0]
end

def goforawalk(maparray,start,end_type,fov_angle,step_distance,iseastwest,target_road_value)

	walkpath = Array.new(){Array.new(2)}
	stepoptions = Array.new(){Array.new(2)}
	theta = 0.0

	walkpath << start.map(&:clone)

    #direction = 0.0 #This is the old value
    if iseastwest
	    direction = Math::PI * 0.5
    else
        direction = -Math::PI * 0.5
    end

	current_location = start.map(&:clone)
	i = 0

	while i < 500 do

        if iseastwest
            if current_location[0] + step_distance*4 > $config_hash["core"]["map_size"][1]*4
                walkpath << [current_location[0]+step_distance*4,current_location[1]]
                break
            end
        else
            if current_location[1] + step_distance*4 > $config_hash["core"]["map_size"][1]*4
                walkpath << [current_location[0],current_location[1]+step_distance*4]
                break
            end
        end

    	# Go straight out for the first step
		if i == 0
            if iseastwest
      		    stepoptions = [[current_location[0]+step_distance*4.0,current_location[1]]]
            else
                stepoptions = [[current_location[0],current_location[1]+step_distance*4.0]]
            end

		else
			stepoptions = getsteplocations(maparray,current_location,direction,fov_angle,step_distance,stepoptions)
		end

		if stepoptions.length() == 0
			break
		end

        #output_path("stepoptions",stepoptions)
		best = findbeststepoption(maparray,stepoptions,current_location,iseastwest,target_road_value)
		theta = Math.atan2(best[1]-current_location[1],best[0]-current_location[0])

		if i == 0
			theta = 0
		end

		theta += Math.atan2(best[1]-current_location[1],best[0]-current_location[0])

		if theta >= Math::PI*2
			#puts "Looping due to theta so break out: #{i}"
			break
		end
		#If we have looped then exit
		if walkpath.include?(best)
			#puts "Looping so break out: #{i}"
			break
		end

		walkpath << best

		direction = - Math.atan2(best[0]-current_location[0],best[1]-current_location[1])
		current_location = best
		i += 1
	end

	return walkpath
end

def findroadarea(road_path)

	road_area = Array.new($config_hash["core"]["map_size"][0]*4){Array.new($config_hash["core"]["map_size"][1]*4,-1)}
	edge = $config_hash["road"]["edge"]["roadedge_width"] * 4.0
    road_width = $config_hash["road"]["width"] * 4.0 * 0.5
    outside_edge = edge
	nextpointsarray = Array.new(){Array.new(2)}

    original_path = findpointsarrayfrompath(road_path)

	nextpointsarray = original_path.map(&:clone)
    for j in 0..nextpointsarray.length()-1 do
        road_area[nextpointsarray[j][0]][nextpointsarray[j][1]] = 0.0
    end
    
	num_iterations = (Math.sqrt(2.0) * (outside_edge + road_width)).floor()
	for i in 0..num_iterations do
		nextpointsarray = growpointsarea(road_area,nextpointsarray)
		for j in 0..nextpointsarray.length()-1 do
            
            distance = finddistancefrompath(nextpointsarray[j],original_path)
            
            if distance <= road_width + outside_edge
                road_area[nextpointsarray[j][0]][nextpointsarray[j][1]] = distance / 4.0
            end
        end
	end

    #Map back all of the default 99999.9 values to -1
    for i in 0..road_area.length()-1 do
        for j in 0..road_area.length()-1 do
            if road_area[i][j] == 99999.9
                road_area[i][j] = -1
            end
        end
    end

	return road_area
end

def apply_terrain_to_road(simple_perlin_map,road_area,terrain)

	target_terrain = Array.new(4)
    width = $config_hash["road"]["width"] * 0.5

	for x in 0..$config_hash["core"]["map_size"][0]*4-1 do
		for y in 0..$config_hash["core"]["map_size"][1]*4-1 do
            
			if !(road_area[x][y] < 0.0) && road_area[x][y] <= width
                
				target_terrain[0] = 255
				target_terrain[1] = 0
				target_terrain[2] = 0
				target_terrain[3] = 0
                
				#If we are going to vary the terrain of the road with terrain_3 then
				if $config_hash["road"].has_key?("terrain")
					if $config_hash["road"]["terrain"]["vary_road_terrain"]

						perlin_value = simple_perlin_map[x][y].to_f()/$config_hash["perlin_noise"]["granularity"]
						target_terrain[0] = ((perlin_value * ($config_hash["road"]["terrain"]["terrain_1_highest_value"]-$config_hash["road"]["terrain"]["terrain_1_lowest_value"]))+$config_hash["road"]["terrain"]["terrain_1_lowest_value"]).floor()
						target_terrain[3] = 255 - target_terrain[0]

						if road_area[x][y] > width - $config_hash["road"]["edge"]["roadedge_width"]
							target_terrain[0] = linear_interpolation($config_hash["road"]["terrain"]["edge_terrain_1_value_255"],target_terrain[0],(width - road_area[x][y])/width)
							target_terrain[3] = 255 - target_terrain[0]
						end
                        
					end
				end


				if road_area[x][y] <= width && road_area[x][y] >= width - $config_hash["road"]["edge"]["roadedge_width"]
                    
					dist_from_edge = (width - road_area[x][y])/$config_hash["road"]["edge"]["roadedge_width"]
					target_terrain[0] = linear_interpolation(terrain[x][y][0],target_terrain[0],dist_from_edge)
					target_terrain[1] = linear_interpolation(terrain[x][y][1],target_terrain[1],dist_from_edge)
					target_terrain[2] = linear_interpolation(terrain[x][y][2],target_terrain[2],dist_from_edge)
					target_terrain[3] = 255 - target_terrain[0] - target_terrain[1] - target_terrain[2]
                    
				end

				terrain[x][y][0] = target_terrain[0]
				terrain[x][y][1] = target_terrain[1]
				terrain[x][y][2] = target_terrain[2]
				terrain[x][y][3] = target_terrain[3]
			end
		end
	end

end

def removeobjectsfromroad(road_area)

	obj_location = Array.new(2)
	obj_index = Array.new(2)
	size_factor = 1.0
	object_array = Array.new(){Array.new(2)}
	store_delete_index = Array.new()

    for j in 0..1 do
        if !$base_hash['world']['levels'].has_key?('1') && j != 0
            next
        end

        for i in 0..$base_hash['world']['levels'][j.to_s()]['objects'].length()-1 do

            if $base_hash['world']['levels'][j.to_s()]['objects'][i] == nil
                puts "Index is wrong"
                next
            end

            obj_location = $base_hash['world']['levels'][j.to_s()]['objects'][i][:position][9..$base_hash['world']['levels'][j.to_s()]['objects'][i][:position].length()-2].split(", ")

            obj_location[0] = (obj_location[0].to_f()/256.0 * 4).floor()/4.0
            obj_location[1] = (obj_location[1].to_f()/256.0 * 4).floor()/4.0

            obj_index[0] = obj_location[0]*4.0.floor()
            obj_index[1] = obj_location[1]*4.0.floor()

            if obj_location[0] < 0.0 || obj_location[0] >= $config_hash["core"]["map_size"][0] || obj_location[1] < 0.0 || obj_location[1] >= $config_hash["core"]["map_size"][1]
                store_delete_index << i
            elsif road_area[obj_index[0]][obj_index[1]] < $config_hash["road"]["width"] * 0.5 && road_area[obj_index[0]][obj_index[1]] >= 0.0
                store_delete_index << i
            end
        end


        for i in 0..store_delete_index.length()-1 do
            $base_hash['world']['levels'][j.to_s()]['objects'].delete_at(store_delete_index[i]-i)
        end
        store_delete_index.clear()
    end

end

def scoreroadwalk(maparray,road_walk,iseastwest,target_road_value)
	score = 0.0
	margin = ($config_hash["road"]["margin_percentage"]*$config_hash["core"]["map_size"][1]*4).floor()
    target_granular = target_road_value * $config_hash["perlin_noise"]["granularity"]

	for i in 0..road_walk.length()-1 do
		if road_walk[i][0] < $config_hash["core"]["map_size"][0]*4 && road_walk[i][1] < $config_hash["core"]["map_size"][1]*4
			score = (score * i + maparray[road_walk[i][0]][road_walk[i][1]])/(i+1).to_f()
            if iseastwest
                if road_walk[i][1] <= margin || road_walk[i][1] >= $config_hash["core"]["map_size"][1]*4 - margin
                    score = (score * i + 0.25 * (maparray[road_walk[i][0]][road_walk[i][1]]-target_granular).abs())/(i+1).to_f()
                else
                    score = (score * i + (maparray[road_walk[i][0]][road_walk[i][1]]-target_granular).abs())/(i+1).to_f()
                end
            else
                if road_walk[i][0] <= margin || road_walk[i][0] >= $config_hash["core"]["map_size"][1]*4 - margin
                    score = (score * i + 0.25 * (maparray[road_walk[i][0]][road_walk[i][1]]-target_granular).abs())/(i+1).to_f()
                else
                    score = (score * i + (maparray[road_walk[i][0]][road_walk[i][1]]-target_granular).abs())/(i+1).to_f()
                end
            end
			
		end
	end

	return score
end

def findbestwalk(maparray,iseastwest,target_road_value)

	walk_options = Array.new(){Array.new(){Array.new(2)}}
	start_options = Array.new(){Array.new(2)}
	margin = ($config_hash["road"]["margin_percentage"]*$config_hash["core"]["map_size"][1]*4).floor()+$config_hash["road"]["width"]*4
	best_score = 0.0
	bext_index = 0
	t = Time.now

	puts "Finding best walk..."
	for i in margin..$config_hash["core"]["map_size"][1]*4-margin-1 do
		if iseastwest
            start_options << [0,i]
        else
            start_options << [i,0]
        end

		road_walk = goforawalk(maparray,start_options[i-margin],0,$config_hash["road"]["fov_angle"]/360*Math::PI*2,$config_hash["road"]["step_distance"],iseastwest,target_road_value)

		score = scoreroadwalk(maparray,road_walk,iseastwest,target_road_value)
        

        #If the road finished at the wrong axis then score 0.0
        if iseastwest
            if road_walk[-1][0] < $config_hash["core"]["map_size"][0]*4
                score = 0.0
            end
        else
            if road_walk[-1][1] < $config_hash["core"]["map_size"][0]*4
                score = 0.0
            end
        end
       
		
		if score > best_score
			best_score = score
			best_index = i - margin
		end
		walk_options << road_walk
    
	end
	puts "Found best walk. Time taken: #{(Time.now-t).floor(3)}s."

    if best_score == 0.0
        puts "Error: no workable road route found."
    end

	road_walk = walk_options[best_index]
	for i in 0..road_walk.length()-1 do
		road_walk[i][0] *= 0.25
		road_walk[i][1] *= 0.25
		
	end

	return road_walk

end

def update_simple_perlin_with_road(simple_perlin_map,road_area)

	if $config_hash["road"].has_key?('road_height')
		road_height = ([$config_hash["road"]["road_height"],1.0].min * $config_hash["perlin_noise"]["granularity"]).floor()
	else
		road_height = (0.8 * $config_hash["perlin_noise"]["granularity"]).floor()
	end
	
    edge = $config_hash["road"]["edge"]["roadedge_width"]
    width = $config_hash["road"]["width"]

	#Note we need to transpose the coordinates for perlin map
	for i in 0..simple_perlin_map.length()-1 do
		for j in 0..simple_perlin_map.length()-1 do

			if road_area[i][j] < width * 0.5 && road_area[i][j] >= 0.0
                simple_perlin_map[i][j] = road_height
				
			elsif road_area[i][j] >= width * 0.5 && road_area[i][j] < width * 0.5 + edge
				simple_perlin_map[i][j] = linear_interpolation(road_height,simple_perlin_map[i][j],(road_area[i][j] - width*0.5) / edge).floor()
			end
			
		end
	end

end

def make_road(simple_perlin_map,terrain)

	start = [0,0]
    leftpath = Array.new(){Array.new(2)}
    rightpath = Array.new(){Array.new(2)}
    road_walk = Array.new(){Array.new(2)}
	width = $config_hash["road"]["width"]
	t = Time.now
	waterpolygonarray = Array.new(){Array.new(2)}
	output_road_area = Array.new(simple_perlin_map.length()){Array.new(simple_perlin_map.length(),0)}
    iseastwest = true
    target_road_value = 0.5

	puts "Making road."
    
	road_walk = findbestwalk(simple_perlin_map,iseastwest,target_road_value)
    
	leftpath = makeperpendicularpath(road_walk,width*0.5,false,nil)
	rightpath = makeperpendicularpath(road_walk,width*0.5,true,nil)

    left_path = makebeziercurve(leftpath,-$config_hash["road"]["bezier_density"],false)
    right_path = makebeziercurve(rightpath,-$config_hash["road"]["bezier_density"],false)

    puts "Found a route for the road, calculating road area."

    road_area = findroadarea(road_walk)

	puts "Defined road, updating perlin map."
	apply_terrain_to_road(simple_perlin_map,road_area,terrain)

	update_simple_perlin_with_road(simple_perlin_map,road_area)
    
	removeobjectsfromroad(road_area)

	if $config_hash.has_key?("lighting")
		if $config_hash["lighting"].has_key?("road_light") && $config_hash["lighting"]["draw_lights"]
			apply_lighting_to_road(road_walk)
		end
	end

	puts "Finished making road. Time taken #{(Time.now-t).floor(2)}s.\n\n"

	if $config_hash["road"]["river"]["make_river"]
		puts "Making river."
		#Add on extra points at the start in order to not have depth 0 on left hand edge
		waterpolygonarray << [right_path[0][0]-4, right_path[0][1]]
		waterpolygonarray << [left_path[0][0]-4, left_path[0][1]]
		# The _path arrays are too dense so remove some points aligned to a truncation factor.
		truncate_path_factor = 3.0
		for i in 0..(left_path.length()/truncate_path_factor).floor()-1
			waterpolygonarray << left_path[(i*truncate_path_factor).floor()]
		end
		waterpolygonarray << [left_path[-1][0]+2, left_path[-1][1]]
		right_path = right_path.reverse()
		for i in 0..(right_path.length()/truncate_path_factor).floor()-1
			waterpolygonarray << right_path[(i*truncate_path_factor).floor()]
		end

		draw_water(waterpolygonarray.flatten(),$config_hash["road"]["river"]["blend_distance"],$config_hash["road"]["river"]["deep_color"],$config_hash["road"]["river"]["shallow_color"],0)


		if $config_hash["road"]["river"].has_key?('water_pattern')
			if $config_hash["road"]["river"]["water_pattern"]["use_water_pattern"]
				draw_pattern(waterpolygonarray.flatten(), $config_hash["road"]["river"]["water_pattern"]["layer"], $config_hash["road"]["river"]["water_pattern"]["texture"], $config_hash["road"]["river"]["water_pattern"]["colour"], $config_hash["road"]["river"]["water_pattern"]["rotation_deg"])
			end
		end

		if $config_hash["road"]["river"].has_key?("side_flowpath") || $config_hash["road"]["river"].has_key?("flowpath") || $config_hash["road"]["river"].has_key?("edge_flowpath")
			make_river_flows(leftpath,rightpath)
		end

		make_river_edges(leftpath,rightpath)
	else
		if $config_hash["road"]["edge"].has_key?('edge_path')
			if $config_hash["road"]["edge"]["edge_path"]["make_path"]

                puts "Making road edges"

                make_path(left_path.reverse(),$config_hash["road"]["edge"]["edge_path"],5)
                make_path(right_path,$config_hash["road"]["edge"]["edge_path"],5)

			end
		end

	end

end

def apply_lighting_to_road(road_walk)

	num_lights = ($config_hash["core"]["map_size"][0]*$config_hash["lighting"]["road_light"]["light_density"]).floor()

	detailed_road = makebeziercurve(road_walk,$config_hash["road"]["bezier_density"],false)
	margin = 0.1

	for i in 0..num_lights do

		index = ( (detailed_road.length()*(1 - 2*margin))/num_lights * i + margin * detailed_road.length() ).floor()

		draw_light(detailed_road[index][0],detailed_road[index][1],$config_hash["lighting"]["road_light"]["light_colour"],$config_hash["lighting"]["road_light"]["light_intensity"],$config_hash["lighting"]["road_light"]["light_radius"],'0')
		
		if $base_hash['world']['levels'].has_key?('1')
			draw_light(detailed_road[index][0],detailed_road[index][1],$config_hash["lighting"]["road_light"]["light_colour"],$config_hash["lighting"]["road_light"]["light_intensity"],$config_hash["lighting"]["road_light"]["light_radius"],'0')
		end
	end

end

## SECTION 6: river definition related functions

def make_intermittent_paths(flowpath, flow_config, islefthand)

    flow_end = flowpath.length()-1
    last_select_flow = 0

    if flowpath.length() < 3
        return
    end

	if flow_config["reverse_direction"] ^ !islefthand
		reverse = true
    else
        reverse = false
	end

	#If we are making small offset paths from a river then 
	if flow_config.has_key?('num_flow_variants')
		num_flow_variants = flow_config["num_flow_variants"]
        flowpaths = Array.new(){Array.new(){Array.new(2)}}
		distance_range = [flow_config["min_distance_from_edge"],$config_hash["road"]["width"]/2.0-flow_config["min_distance_from_middle"]]
		flow_bezier_density = 0.02

		#Make multiple flowpaths noting that we need to smooth this out.
		for i in 0..num_flow_variants-1 do

			delta = i*(distance_range[1]-distance_range[0])/(num_flow_variants-1) + distance_range[0]
			temp_flowpath = makeperpendicularpath(flowpath,delta,islefthand,nil)
			flowpaths << temp_flowpath

		end
	else
		num_flow_variants = 1
        flowpaths = Array.new(){Array.new(){Array.new(2)}}
		flowpaths << flowpath
	end
	
	case flow_config["end_type"]
	when "Shrink"
		end_type = 2
	when "Fade"
		end_type = 0
	else
		end_type = 1
	end

	flow_length_variation = flow_config["flow_length_variation"].floor()
	flowpath_length_in_sq = 0
	if flow_config.has_key?('layer')
		layer = flow_config["layer"]
	else
		layer = 100
	end

	for i in 0..flowpaths[0].length()-2 do
		flowpath_length_in_sq += Math.sqrt((flowpaths[0][i+1][0]-flowpaths[0][i][0])**2+(flowpaths[0][i+1][1]-flowpaths[0][i][1])**2)
	end

	if flow_config.has_key?("flow_min_start")
		last_flow_end = (flow_config["flow_min_start"] * flowpaths[0].length()/flowpath_length_in_sq).floor()
	else
		last_flow_end = 0
	end

	num_flows = (flowpath_length_in_sq*flow_config["flow_density"]/10.0).floor()
    if num_flows == 0
		return
	end

	flowpath_section_length = (flowpaths[0].length()/num_flows.to_f()).floor()
	avg_flow_length = (flow_config["flow_length"]*flowpaths[0].length()/flowpath_length_in_sq).floor()
	if flow_config.has_key?('gap')
		flowpath_gap = (flow_config["gap"]*flowpaths[0].length()/flowpath_length_in_sq).floor()
	else
		flowpath_gap = 0
	end

	if avg_flow_length > flowpath_section_length
		puts "Error in flow config: average flow length #{avg_flow_length} exceeds average section length #{flowpath_section_length}."
		puts "Reduce flow_length or increase flow_density parameters in config file.\n\n"
		exit
	end
	if flowpath_gap > flowpath_section_length - avg_flow_length
		puts "Error in flow config: flowpath_gap #{flowpath_gap} exceeds average section length #{flowpath_section_length} - average flow length #{avg_flow_length}."
		puts "Reduce gap or reduce flow_density parameters in config file.\n\n"
		exit
	end

	for i in 0..10000 do
		flow_length = ((rand(-1.0..1.0)*flow_length_variation+flow_config["flow_length"]) * flowpaths[0].length()/flowpath_length_in_sq).floor()
		if i == 0
			flow_start = rand(0..flowpath_section_length*0.5).floor()
		else
			if flowpath_section_length - flow_length - flowpath_gap > 0
				#flow_start = i * flowpath_section_length + rand(flowpath_gap.to_f()..(flowpath_section_length - flow_length).to_f()).floor()
                if num_flow_variants == 1
                    flow_start = flow_end + rand(-flowpath_gap.to_f()..(flowpath_section_length - flow_length).to_f()).floor()
                else
                    flow_start = flow_end + rand(flowpath_gap.to_f()..(flowpath_section_length - flow_length).to_f()).floor()
                end
			else
				#flow_start = i * flowpath_section_length + flowpath_section_length - flow_length
                flow_start = flow_end + flowpath_section_length - flow_length
			end
		end

		if num_flow_variants == 1
			select_flow = 0
		else
			select_flow = ((1-Math.sqrt(rand()))*num_flow_variants).floor()
            if select_flow == last_select_flow
                select_flow = last_select_flow + 1 % num_flow_variants
            end
            last_select_flow = select_flow
		end

        flow_end = flow_start+flow_length

		if flow_end < flowpaths[0].length()-1 && flow_start >= 0
			smallflowpath = flowpaths[select_flow][flow_start..flow_end]
		elsif flow_start < flowpaths[0].length()-5  && flow_start >= 0
			smallflowpath = flowpaths[select_flow][flow_start..-1]
		else
			break
		end

        if flow_config["randomdirection"]
            reverse = [true,false].sample
        end

		draw_path(smallflowpath, end_type, flow_config["width"], flow_config["texture"],layer,false,'0',!reverse)
	end
end

def make_river_flows(leftpath, rightpath)

	flow_bezier_density = 0.3

	if $config_hash["road"]["river"].has_key?("edge_flowpath")
		reverse_direction = $config_hash["road"]["river"]["edge_flowpath"]["reverse_direction"]

		#Left hand bank
		flowpath = makeperpendicularpath(leftpath,$config_hash["road"]["river"]["edge_flowpath"]["offset"],true,nil)
		draw_path(flowpath, 1, $config_hash["road"]["river"]["edge_flowpath"]["width"], $config_hash["road"]["river"]["edge_flowpath"]["texture"],100,false,'0',!reverse_direction)
	
		#Right hand bank
		flowpath = makeperpendicularpath(rightpath,$config_hash["road"]["river"]["edge_flowpath"]["offset"],false,nil)
		draw_path(flowpath.reverse(), 1, $config_hash["road"]["river"]["edge_flowpath"]["width"], $config_hash["road"]["river"]["edge_flowpath"]["texture"],100,false,'0',!reverse_direction)
	end

	if $config_hash["road"]["river"].has_key?("side_flowpath") || $config_hash["road"]["river"].has_key?("flowpath")
		leftpath_points = makebeziercurve(leftpath,flow_bezier_density,false)
		rightpath_points = makebeziercurve(rightpath,flow_bezier_density,false)
	end

	if $config_hash["road"]["river"].has_key?("side_flowpath")

		flowpath = makeperpendicularpath(leftpath_points,$config_hash["road"]["river"]["side_flowpath"]["offset"],true,nil)
		make_intermittent_paths(flowpath, $config_hash["road"]["river"]["side_flowpath"],true)

		flowpath = makeperpendicularpath(rightpath_points,$config_hash["road"]["river"]["side_flowpath"]["offset"],false,nil)
		make_intermittent_paths(flowpath, $config_hash["road"]["river"]["side_flowpath"],false)
	end

	if $config_hash["road"]["river"].has_key?("flowpath")
		make_intermittent_paths(leftpath_points, $config_hash["road"]["river"]["flowpath"],true)
    	make_intermittent_paths(rightpath_points, $config_hash["road"]["river"]["flowpath"],false)
	end

    

end

def make_river_edges(leftpath, rightpath)

	under_edge_layer = 100
	edge_layer = 100

    if $config_hash["road"]["edge"]["edge_path"].has_key?("make_path")
        if !$config_hash["road"]["edge"]["edge_path"]["make_path"]
            return
        end
    end

    if $config_hash["road"]["edge"]["edge_path"].has_key?("layer")
		edge_layer = $config_hash["road"]["edge"]["edge_path"]["layer"]
    else
        edge_layer = 100
	end

	# If there is a secondary path that we want to place under the main one
	if $config_hash["road"]["edge"].has_key?("under_edge_path")

        if $config_hash["road"]["edge"]["under_edge_path"].has_key?("layer")
            under_edge_layer = $config_hash["road"]["edge"]["under_edge_path"]["layer"]
        else
            under_edge_layer = 100
        end

		offsetleftpath = Array.new(){Array.new(2)}
		offsetrightpath = Array.new(){Array.new(2)}

		#Left hand bank
        offsetleftpath = make_offset_path(leftpath,-$config_hash["road"]["edge"]["under_edge_path"]["offset"],false,false)
        offsetrightpath = make_offset_path(rightpath,$config_hash["road"]["edge"]["under_edge_path"]["offset"],false,false)
        
        draw_path(offsetleftpath, 1, $config_hash["road"]["edge"]["under_edge_path"]["width"], $config_hash["road"]["edge"]["under_edge_path"]["texture"],under_edge_layer,false,'0',$config_hash["road"]["edge"]["under_edge_path"]["reverse_direction"])
        draw_path(offsetrightpath, 1, $config_hash["road"]["edge"]["under_edge_path"]["width"], $config_hash["road"]["edge"]["under_edge_path"]["texture"],under_edge_layer,false,'0',!$config_hash["road"]["edge"]["under_edge_path"]["reverse_direction"])
    end

    draw_path(leftpath, 1, $config_hash["road"]["edge"]["edge_path"]["width"], $config_hash["road"]["edge"]["edge_path"]["texture"],edge_layer,false,'0',$config_hash["road"]["edge"]["edge_path"]["reverse_direction"])
    draw_path(rightpath, 1, $config_hash["road"]["edge"]["edge_path"]["width"], $config_hash["road"]["edge"]["edge_path"]["texture"],edge_layer,false,'0',!$config_hash["road"]["edge"]["edge_path"]["reverse_direction"])
    
end

## SECTION 7: water definition related functions

def make_single_swamp_edge(contours,store_used_indices)

	map_size = $config_hash["core"]["map_size"][0]
	direction = [0,0]
	last_direction = [0,0]
	boundary = [-1.0,map_size+1]
	swamp_edge = Array.new(){Array.new(){Array.new(2)}}
	j = 0

	last_index = 0
	first_index = -1

	#Find the initial edge contour
	for k in 0..contours.length()-1 do
		if !store_used_indices.include?(k) && isedgecontour(contours[k])
			index = 0
			first_index = k
			store_used_indices << first_index
			swamp_edge << contours[first_index]
			break
		end
	end

	if first_index < 0
		return nil
	end

	last_index = first_index
	count = 1

	for k in 0..contours.length()-1 do

		last_point = [contours[last_index][-1][0],contours[last_index][-1][1]]

		for i in 0..2000 do
			direction = get_edge_direction(last_point,boundary)

			if last_direction[0] != direction[0] && last_direction[1] != direction[1] && last_direction != [0,0]
				swamp_edge << [[last_point[0],last_point[1]]]
			end
			last_point[0] += direction[0] * 0.25
			last_point[1] += direction[1] * 0.25
			last_direction[0] = direction[0]
			last_direction[1] = direction[1]

			for j in last_index+1..contours.length()-1 do
				if contours[j][0][0] == last_point[0] && contours[j][0][1] == last_point[1] && isedgecontour(contours[j])
					index = j
				elsif contours[first_index][0][0] == last_point[0] && contours[first_index][0][1] == last_point[1]
					index = -1
				end
			end
			if index != 0
				break
			end
		end

		if index > 0 && !store_used_indices.include?(index)
			store_used_indices << index
			swamp_edge << contours[index]
			count += 1

			last_index = index
			index = 0
		end
		if index < 0
			break
		end
	end

	return swamp_edge.flatten(1)

end

def make_water(water_polygons)

	water_hash_refs = Array.new(water_polygons.length(),0)
	blend_distance = $config_hash["swamp"]["blend_distance"]
	deep_color = $config_hash["swamp"]["deep_color"]
	shallow_color = $config_hash["swamp"]["shallow_color"]
	$base_hash["header"]["editor_state"]["color_palettes"]["deep_water_colors"] << deep_color
	$base_hash["header"]["editor_state"]["color_palettes"]["shallow_water_colors"] << shallow_color

	t = Time.now()
	puts "Making swamp water."

	iscontainedinbynumber = [[],[],[],[]]
	removethese = []
	tempremovethese = []
	check_link_polygon = [-100,-100,[-2.0,-3.0]]
	check_link_polygons = Array.new()
	check_link_surrounding_polygon = [-100,-100,[-2.0,-3.0]]

	iscontainedin = find_polygon_containers(water_polygons)

	#Look for all the swamp water polygons that have no parent and give them a reference, order the rest by number of containing polygons
	for i in 0..iscontainedin.length()-1 do
		if iscontainedin[i].length() == 0
			water_hash_refs[i] = draw_water(water_polygons[i].flatten(),blend_distance,deep_color,shallow_color,0)
			iscontainedinbynumber[0] << i
		else
			iscontainedinbynumber[iscontainedin[i].length()] << i
		end
	end

	#For each type of container with one or more parents, start with the ones with 1, then 2, etc
	for i in 1..iscontainedinbynumber.length()-1 do
		#Start going through the list of lengths, eg one container
		for j in 0..iscontainedinbynumber[i].length()-1 do
			#Check each polygon in the list and remove those that are parents leaving only the index to (ie lowest) polygon itself
			for k in 0..iscontainedin[iscontainedinbynumber[i][j]].length()-1
				if removethese.include?(iscontainedin[iscontainedinbynumber[i][j]][0])
					iscontainedin[iscontainedinbynumber[i][j]] = iscontainedin[iscontainedinbynumber[i][j]].drop(1)
				else
					tempremovethese << iscontainedin[iscontainedinbynumber[i][j]][0]
					if i != 2
						water_hash_refs[iscontainedinbynumber[i][j]] = draw_water(water_polygons[iscontainedinbynumber[i][j]].flatten(),0,"00000000","00000000",water_hash_refs[iscontainedin[iscontainedinbynumber[i][j]][0]])
					else
						water_hash_refs[iscontainedinbynumber[i][j]] = draw_water(water_polygons[iscontainedinbynumber[i][j]].flatten(),blend_distance,deep_color,shallow_color,0)
					end
				end
			end
		end
		removethese << tempremovethese
		removethese = removethese.flatten()
	end

	containingpolygon = Array.new(iscontainedin.length(),[])

	#For each polygon
	for i in 0..iscontainedin.length()-1 do
		#puts "iscontainedin[#{i}]:\t#{iscontainedin[i]}\twater_references: #{water_hash_refs[i]}\tfirst point: #{water_polygons[i][0]}"

		#Build an array of polygons by containing polygon
		if iscontainedin[i][0] != nil
			if containingpolygon[iscontainedin[i][0]].length() == 0
				containingpolygon[iscontainedin[i][0]] = Array.new()
			end
			containingpolygon[iscontainedin[i][0]] << i

		end
	end

	for i in 1..iscontainedinbynumber.length()-1 do
		#puts "iscontainedinbynumber[#{i}]: #{iscontainedinbynumber[i]}"
	end

	alreadymergedpolygons = Array.new()
	#For each polygon containing other polygons (one level down)
	for i in 0..containingpolygon.length()-1 do
		#If top level polygon with no contained polygons then just draw and don't look to merge. Noting we are hoping there are no third level polygons
		if containingpolygon[i].length() == 0
			next
		end
		#If a second level polygon then just draw and don't look to merge. Noting we are hoping there are no third level polygons
		if iscontainedinbynumber[2] != 0
			skip = false
			for n in 0..containingpolygon[i].length()-1 do
				if iscontainedinbynumber[2].include?(containingpolygon[i][n])
					skip = true
				end
			end
			if skip
				next
			end
		end


		#puts "containingpolygon[#{i}]: #{containingpolygon[i]}"
		
		#Arrange the polygons in order of least max x value to avoid drilling through other polygons
		polygon_order = find_polygons_in_ascending_order_max_x(containingpolygon[i],water_polygons)
		
		#Start with the leftmost one and seek the others
		new_polygon = water_polygons[polygon_order[0]]
		new_polygon_index = polygon_order[0]
		j = 1

		for m in 0..1000 do
			if j >= polygon_order.length()
				break
			end
			if !alreadymergedpolygons.include?(polygon_order[j])
				check_link_polygon = check_link_polygon_with_other_polygon(new_polygon,water_polygons[polygon_order[j]])
				
				#If found a match then merge with that polygon and remove it from future comparisons. Check the surrounding polygon to ensure we don't tunnel through it.
				if check_link_polygon[1] > 0 then
                    check_link_polygons << [j,check_link_polygon]
				end
			end
			#If we have checked all the polygons then merge with the containingpolygon and reset to the next available polygon that hasn't been used already
			if j == polygon_order.length()-1
				#If new_polygon is the last one in the list then break
				if new_polygon_index == polygon_order[-1]
					break
				#End of the loop so merge the current polygon and find the next available polygon index, ie the next one after the current one that hasn't been used yet.
				else
                    check_link_surrounding_polygon = check_link_polygon_with_other_polygon(new_polygon,water_polygons[i])
					#Find the best linked polygon
                    if check_link_polygons.length()>0 then
                        best_link_index = -1
                        best_link_point = 2000.0
                        for n in 0..check_link_polygons.length()-1 do
                            if check_link_polygons[n][1][2][0] < best_link_point
                                best_link_index = n
                                best_link_point = check_link_polygons[n][1][2][0]
                            end
                        end
                        #If you find out then merge that one and reset the j value 
                        if check_link_polygons[best_link_index][1][2][0] < check_link_surrounding_polygon[2][0]
                            new_polygon = merge_polygons(new_polygon,water_polygons[polygon_order[check_link_polygons[best_link_index][0]]],check_link_polygons[best_link_index][1][0],check_link_polygons[best_link_index][1][1],check_link_polygons[best_link_index][1][2],true)
					        alreadymergedpolygons << polygon_order[check_link_polygons[best_link_index][0]]
                            check_link_polygons.clear()
                            for k in 0..polygon_order.length()-1 do
                                #Look for a new start point to check the remaining polygons as we have new points remembering to exclude the current one
                                if !alreadymergedpolygons.include?(polygon_order[k]) && new_polygon_index != polygon_order[k]
                                    j = k
                                    break
                                end
                            end
                        end
                    else
                    
                        #Merge the current polygon but need to check the best link to the surrounding polygon as we may have added one this cycle
                        water_polygons[i] = merge_polygons(new_polygon,water_polygons[i],check_link_surrounding_polygon[0],check_link_surrounding_polygon[1],check_link_surrounding_polygon[2],false)
                        alreadymergedpolygons << new_polygon_index
                        for k in 0..polygon_order.length()-1 do
                            if !alreadymergedpolygons.include?(polygon_order[k]) && new_polygon_index != polygon_order[k]
                                new_polygon_index = polygon_order[k]
                                new_polygon = water_polygons[polygon_order[k]]
                                j = k
                                break
                            end
                        end
                    end
				end
			end

			
			j += 1

		end

		if !alreadymergedpolygons.include?(new_polygon_index)
			check_link_polygon = check_link_polygon_with_other_polygon(new_polygon,water_polygons[i])
			water_polygons[i] = merge_polygons(new_polygon,water_polygons[i],check_link_polygon[0],check_link_polygon[1],check_link_polygon[2],false)
			alreadymergedpolygons << new_polygon_index
			#puts "Merging #{new_polygon_index} into #{i} as done with all polygons contained within #{i}"
		end
	end


	#Overlay a pattern if one is defined

	if $config_hash["swamp"].has_key?('water_pattern') && true
        texture = $config_hash["swamp"]["water_pattern"]["texture"]
        colour_str = $config_hash["swamp"]["water_pattern"]["colour"]
        rotation = $config_hash["swamp"]["water_pattern"]["rotation_deg"]

        #Draw out the water patterns where needed.
        for i in 0..iscontainedin.length()-1 do
            #puts "Drawing water patterns: \tiscontainedin[#{i}]:\t#{iscontainedin[i]}\t isalreadymerged: #{alreadymergedpolygons.include?(i)}"
            if !alreadymergedpolygons.include?(i)
                draw_pattern(water_polygons[i].flatten(), 100, texture, colour_str, rotation)
            end
        end
    end
	puts "Water complete. Time taken: #{(Time.now()-t).floor(2)}s.\n"

end

def make_water_edges(contours)

	water_edges = Array.new(){Array.new(){Array.new(2)}}
	store_used_indices = Array.new()
	water_polygons = Array.new()
	count_edge_contours = 0
	t = Time.now()

	puts "Finding edge contours and merging."

	for i in 0..contours.length()-1 do
		if isedgecontour(contours[i])
			count_edge_contours += 1
		end
	end

    #If there are no edge conotours then we are in an island situation so add a surround.
    if count_edge_contours == 0
		outside_edge = [[-1.0,-1.0],[-1.0,$config_hash["core"]["map_size"][0]+1.0],[$config_hash["core"]["map_size"][0]+1.0,$config_hash["core"]["map_size"][0]+1.0],[$config_hash["core"]["map_size"][0]+1.0,-1.0]]
		outside_edge = transposearrayofcoords(outside_edge)
        water_edges << outside_edge
        water_polygons << outside_edge
	else
		for i in 0..500 do
			water_edges[i] = make_single_swamp_edge(contours,store_used_indices)

			water_polygons << water_edges[i]

			if store_used_indices.length() >= count_edge_contours
				break
			end

		end

		puts "Finished merging edge contours. Time taken: #{(Time.now()-t).floor(2)}s."
		
	end

	for i in count_edge_contours..contours.length()-1

		water_polygons << contours[i]
		water_edges << contours[i]

	end

	make_water(water_polygons)

	return water_edges

end

def make_water_areas(simple_perlin_map)

	level = $config_hash["swamp"]["water_level"]
	offset_path = Array.new(){Array.new()}
	detailed_path = Array.new(){Array.new()}
	if $config_hash["swamp"].has_key?('contour_filter')
		filter = $config_hash["swamp"]["contour_filter"]
	else
		filter = 3
	end

	t = Time.now()
	puts "Making swamp."

	contours = make_elevation_contours(simple_perlin_map,level*$config_hash["perlin_noise"]["granularity"].floor(),filter)
	
	if $config_hash["swamp"].has_key?('smooth_water_edges')
		if $config_hash["swamp"]["smooth_water_edges"]
			for i in 0..contours.length()-1 do
				if isedgecontour(contours[i])
					contours[i] = makebeziercurve(contours[i],0.3,false)
				else
					contours[i] = makebeziercurve(contours[i],0.3,true)
				end
			end
		end
	end
	
	swamp_edges = make_water_edges(contours)

	if $config_hash["swamp"].has_key?('water_edges')
		for i in 0..swamp_edges.length()-1 do

			#If we have created a default surrounding edge for an island then don't draw a path for it
			if i == 0 && swamp_edges[i][0] == [-1.0,-1.0]
                next
            end
			isedge = isedgecontour(swamp_edges[i])

			for j in 0..$config_hash["swamp"]["water_edges"].length()-1 do

				if $config_hash["swamp"]["water_edges"][j].has_key?('offset')
					offset_path = make_offset_path(swamp_edges[i],$config_hash["swamp"]["water_edges"][j]["offset"],false,!isedge)
					if $config_hash["swamp"]["water_edges"][j]["type"] == "Intermittent"
						detailed_path = makebeziercurve(offset_path,0.1,false)
						make_intermittent_paths(detailed_path, $config_hash["swamp"]["water_edges"][j],true)
					else
						draw_path(offset_path, 1, $config_hash["swamp"]["water_edges"][j]["width"], $config_hash["swamp"]["water_edges"][j]["texture"], $config_hash["swamp"]["water_edges"][j]["layer"], !isedge,'0',!$config_hash["swamp"]["water_edges"][j]["reverse_direction"])
					end

				else
					draw_path(swamp_edges[i].reverse(), 1, $config_hash["swamp"]["water_edges"][j]["width"], $config_hash["swamp"]["water_edges"][j]["texture"], $config_hash["swamp"]["water_edges"][j]["layer"], !isedge,'0',!$config_hash["swamp"]["water_edges"][j]["reverse_direction"])
				end

			end
		end

	end

	puts "Swamp complete. Time taken: #{(Time.now()-t).floor(2)}s.\n\n"

	return

end

## SECTION 8: hill definition related functions

def make_elevation_contours(perlin,level,filter)

	contours = Array.new(){Array.new(){Array.new(2)}}

	flatmap = collapseperlintobinary(perlin,level)
	
	case_array = calculate_ms_case(flatmap)
	
	contours = findcontours(case_array, filter)

	return contours

end

def calculate_angle_between_3pts(point_1,point_2,point_3)

    theta = 0.0
    vector_1 = [point_2[0]-point_1[0],point_2[1]-point_1[1]]
    vector_2 = [point_3[0]-point_2[0],point_3[1]-point_2[1]]

    dotproduct = vector_1[0] * vector_2[0] + vector_1[1] * vector_2[1]
    value = dotproduct / (Math.sqrt(vector_1[0]**2 + vector_1[1]**2) * Math.sqrt(vector_2[0]**2 + vector_2[1]**2))
    if value > 1.0
        value = 1.0
    elsif value < -1.0
        value = -1.0
    end
    
    theta = Math.acos(value)

    #puts "theta: #{(theta*360.0/(2*Math::PI)).floor(2)}\t point_1: #{point_1[0].floor(2)},#{point_1[1].floor(2)}\t point_2: #{point_2[0].floor(2)},#{point_2[1].floor(2)}\t point_3: #{point_3[0].floor(2)},#{point_3[1].floor(2)}"

    return theta

end

#Removes all path angles less than 90 degrees
def removesharpangles(path)

	minimum_angle = 80.0
	min_angle_radians = minimum_angle * Math::PI / 180.0

    isedge = isedgecontour(path)

    i = 1
    for j in 0..path.length()*2 do
        theta = calculate_angle_between_3pts(path[(i - 1) % path.length()],path[i],path[(i + 1) % path.length()])
        if theta.abs() > min_angle_radians || theta.nan?
            path.delete_at(i)
            #Jump back to ensure we haven't missed the connection to the previous point and check that the path is non-zero
			if path.length() > 0
            	i = (i - 1) % path.length()
			else
				break
			end
        else
            i = (i + 1) % path.length()
        end
        if isedge && i >= path.length()-2
            i = 1
            break
        end
    end

end

def make_path(path_array,path_hash,default_min_length)

	offsetpaths = []

	if path_hash.has_key?('contour_filter')
		filter = path_hash["contour_filter"]
	else
		filter = 3
	end
	
	if path_hash.has_key?('min_length')
		min_length = path_hash["min_length"]
	else
		if default_min_length != nil
			min_length = default_min_length
		else
			min_length = 10
		end
	end

	if path_array.length() > min_length.to_f() * filter

		isedge = isedgecontour(path_array)
		path_array = filter_path(path_array, filter)
		removesharpangles(path_array)
		path_array = normalise_path(path_array)

        if path_hash.has_key?('offset')

			temp_array = makebeziercurve(path_array,0.25,!isedge)		
			offsetpath = make_offset_path(temp_array,path_hash["offset"],true,!isedge)
			path_array = filter_path(offsetpath, 4)
			
		end

		if path_hash.has_key?('offset_paths')
			
			temp_array = makebeziercurve(path_array,0.25,!isedge)
			for i in 0..path_hash["offset_paths"].length()-1 do

				if path_hash["offset_paths"][i].has_key?('contour_filter')
					offsetfilter = path_hash["offset_paths"][i]["contour_filter"]
				else
					offsetfilter = 3
				end

				offsetpaths << make_offset_path(temp_array,path_hash["offset_paths"][i]["offset"],true,!isedge)
				offsetpaths[i] = filter_path(offsetpaths[i], offsetfilter)
			end
		end
		
		if path_hash.has_key?('type')
			if path_hash["type"] == "Intermittent"
				if !path_hash.has_key?('smoothing_factor')
					path_hash["smoothing_factor"] = 1
				end
				if !path_hash.has_key?('detailed_factor')
					path_hash["detailed_factor"] = 1.0
				end
				path_array = filter_path(path_array, path_hash["smoothing_factor"])	
				path_array = makebeziercurve(path_array,path_hash["detailed_factor"],!isedge)

				if path_hash.has_key?('offset')
					path_array = make_offset_path(path_array,path_hash["offset"],true,!isedge)
				end
				
				make_intermittent_paths(path_array, path_hash,!path_hash["reverse_direction"])
			else
				draw_path(path_array,1,path_hash["width"],path_hash["texture"],path_hash["layer"],!isedge,'0',!path_hash["reverse_direction"])
			end
		else
			
			draw_path(path_array,1,path_hash["width"],path_hash["texture"],path_hash["layer"],!isedge,'0',!path_hash["reverse_direction"])
			
		end

		if path_hash.has_key?('offset_paths')
			for i in 0..path_hash["offset_paths"].length()-1 do

				if offsetpaths[i].length() > 2
					texture = path_hash["offset_paths"][i]["texture"]
					draw_path(offsetpaths[i],1,path_hash["offset_paths"][i]["width"],texture,path_hash["offset_paths"][i]["layer"],!isedge,'0',!path_hash["offset_paths"][i]["reverse_direction"])
	
				end
			end
			
		end
	end

end

def make_hills(simple_perlin_map)

    offsetpath = Array.new(){Array.new(2)}

	t = Time.now()

	puts "Making hills."

	for j in 0..$config_hash["hills"]["hill_list"].length()-1 do

        level = $config_hash["hills"]["hill_list"][j]["level"]

		puts "Making hills with perlin height: #{level.floor(2)}."
		
		contours = make_elevation_contours(simple_perlin_map,level*$config_hash["perlin_noise"]["granularity"].floor(),1)
		
		for i in 0..contours.length()-1 do

            make_path(contours[i],$config_hash["hills"]["hill_list"][j],$config_hash["hills"]["min_length"])
            
		end
        contours.clear
	end

	puts "Finished hills. Time taken: #{(Time.now()-t).floor(2)}s."

end

## SECTION 9: perlin modification related functions

#Update a binary map to set all values bounded by the path to have value 0
def updateflatmappoint(flatmap,i,j,update_backlog)

	point = Array.new(2)

	if isvalidindex(i,j)
		if flatmap[i][j] < 2
			point = [i,j]
			if !update_backlog.include?(point)
				update_backlog << point
				return true
			else
				return false
			end
		else
			return false
		end
	else
		return false
	end

end

# Find a location to start filling in the canyon uplift area, run around the edge of the map until we find a place that is not on the contour edge
def findvalidfillstartpoint(flatmap, path, contour_type)

	delta_direction = [0,0]
	current = [-1,-1]
	max_index = $config_hash["core"]["map_size"][0]*4-1

	case contour_type
	when "ew", "es", "ee"
		current = [max_index,(path[0][1]*4).floor()]
		delta_direction = [0,1]
	when "sw", "ss"
		current = [(path[0][0]*4).floor(),max_index]
		delta_direction = [-1,0]
	when "we", "wn", "ww"
		current = [0,(path[0][1]*4).floor()]
		delta_direction = [0,-1]
	when "ne", "nn"
		current = [(path[0][0]*4).floor(),0]
		delta_direction = [1,0]
	end

	for k in 0..10000 do
		if flatmap[current[0]][current[1]] != 4
			break
		end
		# Move in the current direction

		current[0] += delta_direction[0]
		current[1] += delta_direction[1]
		
		# Check if we have hit a corner in which case turn
		if current == [0,0] || current == [max_index,0] || current == [max_index,max_index] || current == [0,max_index]
			delta_direction = [-delta_direction[1],delta_direction[0]]
		end

	end

	return current
end

# Modify the flatmap array so that all of the non-canyon areas are filled with "3" noting that all the contours are marked with a "4"
def modify_flatmap_area(flatmap, path, value, contour_type)

	update_backlog = Array.new{Array.new(2)}

	current = findvalidfillstartpoint(flatmap, path, contour_type)

	flatmap[current[0]][current[1]] = 0
	
	for i in 0..1000000 do

		checkpoint = false

		checkpoint = updateflatmappoint(flatmap,current[0]-1,current[1],update_backlog)
		checkpoint = updateflatmappoint(flatmap,current[0]+1,current[1],update_backlog) || checkpoint
		checkpoint = updateflatmappoint(flatmap,current[0],current[1]-1,update_backlog) || checkpoint
		checkpoint = updateflatmappoint(flatmap,current[0],current[1]+1,update_backlog) || checkpoint

		if checkpoint
			current = [update_backlog[0][0],update_backlog[0][1]]
			flatmap[current[0]][current[1]] = 3
			update_backlog.delete_at(0)
		else
			if update_backlog.length()>0
				current = [update_backlog[0][0],update_backlog[0][1]]
				flatmap[current[0]][current[1]] = 3
				update_backlog.delete_at(0)
			else
				break
			end
		end

	end

end

# Remove any contours except ones which bound the nearest to the centre, so we define the areas bounded by those contours as the "high" areas.
def trim_to_nearest_to_centre(closest, contours, axis_str)

	if closest[axis_str].length() == 0
		return
	end

	firstlastpoints = Array.new(){Array.new(){Array.new(2)}}

	for i in 0..contours.length()-1 do

		firstlastpoints << [[contours[i][0][0],contours[i][0][1]],[contours[i][-1][0],contours[i][-1][1]]]

	end

	corner_str = {"ss" => ["es","sw"], "nn" => ["ne","wn"], "ee" => ["ne","es"], "ww" => ["wn","sw"]}
	valid_array = Array.new()

	case axis_str
	when "nn", "ss"
		axis = 0
	when "ee", "ww"
		axis = 1
		if closest["ew"] > -2
			# Set up the edge values to be corners of the map if needed
			if axis_str == "ee"
				firstlastpoints[closest["ew"]][-1][1] = $config_hash["core"]["map_size"][0]*4
			else
				firstlastpoints[closest["ew"]][0][1] = $config_hash["core"]["map_size"][0]*4
			end
			valid_array << closest["ew"]
		end
		if closest["we"] > -2
			# Set up the edge values to be corners of the map if needed
			if axis_str == "ww"
				firstlastpoints[closest["we"]][-1][1] = 0
			else
				firstlastpoints[closest["we"]][0][1] = 0
			end
			valid_array << closest["we"]
		end

	end
	
	if closest[corner_str[axis_str][0]] > -2
		valid_array << closest[corner_str[axis_str][0]]
	end
	if closest[corner_str[axis_str][1]] > -2
		valid_array << closest[corner_str[axis_str][1]]
	end

	# Count up the number of edge contours or type "ew", "we", "ne", "wn", "ne" or "es" to compare to
	compare_count = valid_array.length()

	# Add in all the contours of the type we are looking at
	valid_array << closest[axis_str]
	valid_array.flatten!()

	# Start at the first contour of type axis_str, ignoring the others
	for i in compare_count..valid_array.length()-1 do
		# Then check all of the other contours to see which contain contour i
		for j in 0..valid_array.length()-1 do

			value = firstlastpoints[valid_array[i]][0][axis]
			edge_value_1 = firstlastpoints[valid_array[j]][0][axis]
			edge_value_2 = firstlastpoints[valid_array[j]][-1][axis]
			# check using the edge values of contour, j, if contour i is contained in contour j then delete it.
			if ((value < edge_value_1) && (value > edge_value_2)) || ((value > edge_value_1) && (value < edge_value_2))
				#Not valid as the contour is contained in another contour
				closest[axis_str].delete(valid_array[i])
				
				break
			end
		end
	end
	valid_array.clear()

end

def findcontourtype(contours)

	contour_type = Array.new()

	for i in 0..contours.length()-1 do
		type_str = "xx"
		if contours[i][0][0] < 0.0
			type_str[0] = "w"
		elsif contours[i][0][0] > $config_hash["core"]["map_size"][0]
			type_str[0] = "e"
		elsif contours[i][0][1] < 0.0
			type_str[0] = "n"
		elsif contours[i][0][1] > $config_hash["core"]["map_size"][0]
			type_str[0] = "s"
		end
		if contours[i][-1][0] < 0.0
			type_str[1] = "w"
		elsif contours[i][-1][0] > $config_hash["core"]["map_size"][0]
			type_str[1] = "e"
		elsif contours[i][-1][1] < 0.0
			type_str[1] = "n"
		elsif contours[i][-1][1] > $config_hash["core"]["map_size"][0]
			type_str[1] = "s"
		end
		contour_type << type_str
	end

	return contour_type

end

def findclosestcontours(contour_type,contours)

	closest = Hash.new
	min_contour_length = 20
	#Note we have not created all possibilities as assuming that a road bisects the map east-west
	closest = {"we" => -2,"ew" => -2,"wn" => -2,"ne" => -2,"es" => -2,"sw" => -2,"ww" => [],"ee" => [],"nn" => [],"ss" => []}

	#Keep the farthest south "we", farthest north "ew"
	for i in 0..contours.length()-1 do

		if contours[i].length() < min_contour_length
			next
		end

		if contour_type[i] == "ww" || contour_type[i] == "ee" || contour_type[i] == "nn" || contour_type[i] == "ss"
			closest[contour_type[i]] << i
			next
		end

		case contour_type[i]
		when "we", "wn"
			if closest[contour_type[i]] > -2
				#Check highest y value
				if contours[closest[contour_type[i]]][0][1] > contours[i][0][1]
					closest[contour_type[i]] = i
				end
			else
				closest[contour_type[i]] = i
			end
		when "ew", "es"
			if closest[contour_type[i]] > -2
				#Check lowest y value
				if contours[closest[contour_type[i]]][0][1] <= contours[i][0][1]
					closest[contour_type[i]] = i
				end
			else
				closest[contour_type[i]] = i
			end
		when "ne"
			if closest[contour_type[i]] > -2
				#Check highest y value
				if contours[closest[contour_type[i]]][-1][1] > contours[i][-1][1]
					closest[contour_type[i]] = i
				end
			else
				closest[contour_type[i]] = i
			end
		when "sw"
			if closest[contour_type[i]] > -2
				#Check lowest y value
				if contours[closest[contour_type[i]]][-1][1] <= contours[i][-1][1]
					closest[contour_type[i]] = i
				end
			else
				closest[contour_type[i]] = i
			end
		end

	end

	return closest

end

def findvalidclosestcontours(closest,contours)

	use_contours_indices = Array.new()

	closest.each do | key, value |
		
		case key
		when "ew"
			if value > -2
				use_contours_indices << closest["ew"]
				closest["ss"] = []
				closest["sw"] = -2
				closest["es"] = -2
			end
		when "we"
			if value > -2
				use_contours_indices << closest["we"]
				closest["nn"] = []
				closest["wn"] = -2
				closest["ne"] = -2
			end
		when "nn","ee","ss","ww"
			trim_to_nearest_to_centre(closest, contours, key)
			
			for j in 0..value.length()-1 do
				use_contours_indices << value[j]
			end
		else
			if value > -2
				use_contours_indices << value
			end
		end
	end
	return use_contours_indices
end

def findcontoursofcanyon(contours,flatmap)

	i = 0
	for n in 0..contours.length()-1 do
		if !isedgecontour(contours[i])
			contours.delete_at(i)
        else
			i += 1
        end
        if i == contours.length()
            break
        end
	end

	contour_type = findcontourtype(contours)

	closest = findclosestcontours(contour_type,contours)

	use_contours = findvalidclosestcontours(closest,contours)

	return use_contours

end

def modify_perlin_canyon_by_level(simple_perlin_map,level)

	contours = Array.new(){Array.new(){Array.new(2)}}
	use_contours = Array.new()
	contour_type = Array.new()
	change_edges = Array.new()
	distance_from_edges = Array.new(simple_perlin_map.length()){Array.new(simple_perlin_map.length(),99)}
	granularity = $config_hash["perlin_noise"]["granularity"]
	filter = 1
	
	flatmap = collapseperlintobinary(simple_perlin_map,level*$config_hash["perlin_noise"]["granularity"].floor())
    
	case_array = calculate_ms_case(flatmap)
	
	contours = findcontours(case_array, filter)

	use_contours = findcontoursofcanyon(contours,flatmap)
	contour_type = findcontourtype(contours)

	#Make the usable contours block the modification
	for j in 0..use_contours.length()-1 do
		for i in 0..contours[use_contours[j]].length()-1 do
			if isvalidindex(contours[use_contours[j]][i][0]*4,contours[use_contours[j]][i][1]*4)
				flatmap[contours[use_contours[j]][i][0]*4][contours[use_contours[j]][i][1]*4] = 4
			end
		end
	end
    
	# Fill in the areas of the map defined by the use_contours list
	for i in 0..use_contours.length()-1 do
		modify_flatmap_area(flatmap, contours[use_contours[i]], 1, contour_type[use_contours[i]])
	end

	#Find the highest perlin value in the area we are going to modify
	highest_value = 0
	lowest_value = granularity
	for i in 0..flatmap.length()-1 do
		for j in 0..flatmap.length()-1 do
			if flatmap[i][j] == 4 || flatmap[i][j] == 3
				if highest_value < simple_perlin_map[i][j]
					highest_value = simple_perlin_map[i][j]
				end
				if lowest_value > simple_perlin_map[i][j]
					lowest_value = simple_perlin_map[i][j]
				end
			end
			if flatmap[i][j] == 4
				change_edges << [i,j]
				distance_from_edges[i][j] = 0
			end
		end
	end
	# The number of entries distant from the edge in raised area that we are going to try and smooth to the edge for. 16 seems about right but not very efficient.
	distance_to_smooth = 16
	# For each edge line
	for k in 0..change_edges.length()-1 do
		for i in -distance_to_smooth..distance_to_smooth do
			for j in -distance_to_smooth..distance_to_smooth do
				if isvalidindex(change_edges[k][0]+i,change_edges[k][1]+j)
					if flatmap[change_edges[k][0]+i][change_edges[k][1]+j] == 3
						distance = finddistancebetweenpoints([change_edges[k][0]+i,change_edges[k][1]+j],change_edges[k])
						if distance_from_edges[change_edges[k][0]+i][change_edges[k][1]+j] > distance
							distance_from_edges[change_edges[k][0]+i][change_edges[k][1]+j] = distance.floor()
						end
					end
				end
			end
		end
	end
	# Modify perlin values within the modification area
	for i in 0..flatmap.length()-1 do
		for j in 0..flatmap.length()-1 do
			if flatmap[i][j] == 3

				value_option_1 = ((simple_perlin_map[i][j] - lowest_value)/(highest_value-lowest_value).to_f()) * (level*granularity - lowest_value) + lowest_value
				value_option_1 = [value_option_1.floor(),lowest_value].max()
				if distance_from_edges[i][j] > 16			
					simple_perlin_map[i][j] = value_option_1
				else
					value_option_2 =  level*granularity - ((highest_value - simple_perlin_map[i][j])/(highest_value-lowest_value).to_f()) * (level*granularity - simple_perlin_map[i][j]).abs()
					simple_perlin_map[i][j] = ( value_option_2 * (distance_to_smooth - distance_from_edges[i][j]) + value_option_1 * distance_from_edges[i][j] ) / distance_to_smooth.to_f()
					simple_perlin_map[i][j] = [simple_perlin_map[i][j].floor(),lowest_value].max()
				end

			end
			next
			#Only modify if type 3 or 4, ie within the mod area
			if flatmap[i][j] == 4 || flatmap[i][j] == 3

				# Core modification, push all values above level and ensure they are no smaller than the previous min number
				newvalue =  level*granularity - ((highest_value - simple_perlin_map[i][j])/(highest_value-lowest_value).to_f()) * (level*granularity - simple_perlin_map[i][j]).abs()
				newvalue = newvalue.floor()
				simple_perlin_map[i][j] = newvalue

			end
			
		end
	end
	#Smooth the edge values as they are creating discontinuities
	for i in 0..change_edges.length()-1 do
		valid_count = 0
		average = 0
		for k in 0..4 do
			if isvalidindex(change_edges[i][0]-2+k,change_edges[i][1])
				if flatmap[change_edges[i][0]-2+k][change_edges[i][1]] != 4
					valid_count += 1 
					average += simple_perlin_map[change_edges[i][0]-2+k][change_edges[i][1]]
				end
			end
			if isvalidindex(change_edges[i][0],change_edges[i][1]-2+k)
				if flatmap[change_edges[i][0]][change_edges[i][1]-2+k] != 4
					valid_count += 1 
					average += simple_perlin_map[change_edges[i][0]][change_edges[i][1]-2+k]
				end
			end
		end
		if valid_count > 0
			simple_perlin_map[change_edges[i][0]][change_edges[i][1]] = (average / valid_count.to_f()).floor()
		end
	end
end

def modify_perlin_canyon(simple_perlin_map)

	t = Time.now()

    puts "Modifying perlin map to make a canyon."

	if !$config_hash["perlin_noise"]["modify"].has_key?('levels')
		puts "No levels entry in $config_hash['perlin_noise']['modify']."
		puts "No modification of perlin in modify_perlin_canyon function."
		return
	end

	# Make sure we always do this in the right order, ie highest value first.
	$config_hash["perlin_noise"]["modify"]["levels"].sort!().reverse!()
	
	for i in 0..$config_hash["perlin_noise"]["modify"]["levels"].length()-1 do
		puts "Modifying perlin map for Canyon Level: #{$config_hash["perlin_noise"]["modify"]["levels"][i]*$config_hash["perlin_noise"]["granularity"]}"
		modify_perlin_canyon_by_level(simple_perlin_map,$config_hash["perlin_noise"]["modify"]["levels"][i])
	end

	puts "Perlin modification complete. Time taken: #{(Time.now()-t).floor(2)}s."

end

def modify_perlin_island(pixels)

    new_pixels = Array.new(pixels.length){Array.new(pixels.length,nil)}
    granularity = $config_hash["perlin_noise"]["granularity"]
    radius = $config_hash["perlin_noise"]["modify"]["radius_fraction"]
    max_pixels = 0.0
    max_new_pixels = 0.0
    t = Time.now()

    puts "Modifying perlin map to make an island."

	for i in 0..pixels.length()-1 do
        for j in 0..pixels.length()-1 do
            distance_sqd = ((i - pixels.length()*0.5) ** 2 + (j - pixels.length()*0.5) ** 2) / pixels.length() ** 2
            if distance_sqd > radius**2
                new_pixels[i][j] = 1
            else
                value = radius - Math.sqrt(distance_sqd)
                new_pixels[i][j] = (pixels[i][j] * value).floor()
            end
            if pixels[i][j] > max_pixels
                max_pixels = pixels[i][j]
            end
            if new_pixels[i][j] > max_new_pixels
                max_new_pixels = new_pixels[i][j]
            end
        end
    end

    weight = max_pixels/max_new_pixels

    for i in 0..pixels.length()-1 do
        for j in 0..pixels.length()-1 do
            pixels[i][j] = (granularity - new_pixels[i][j] * weight).floor()
        end
    end

    if $config_hash["perlin_noise"]["modify"].has_key?('outputppmfile')
		if $config_hash["perlin_noise"]["modify"]["outputppmfile"]
			outputppm($config_hash["perlin_noise"]["modify"]["output_modify_perlin_ppm_filename"],pixels,pixels.length(),pixels.length(),$config_hash["perlin_noise"]["granularity"])
		end
	end

    puts "Perlin modification complete. Time taken: #{(Time.now()-t).floor(2)}s."

end

## START OF MAIN CODE

config_file_str = "config.json"
output_file_str = "undefined"
use_perlin_store = false
no_river_opts = false
override_mapsize = 0
override_perlinsize = 0
no_lights = false

# Parse options
parser = OptionParser.new do |opts|
	opts.banner = "Usage: randomforest.rb [options]"

  opts.on('-c config_file', 'Specify config file to use') do |config_file|
		config_file_str = config_file;
  end

	opts.on('-s', 'Forces the use of a stored perlin file if it exists even if the config file does not') do
		use_perlin_store = true;
	end

	opts.on('-o output_file', 'Specifies the output file name including the .dungeondraft_map extension') do |output_file|
		output_file_str = output_file;
  end

	opts.on('-d', 'Forces the code to only create a road not a river even if the config file does not specify this') do
		no_river_opts = true;
	end

	opts.on('-m map_size', 'Forces the code to overwrite the map size in the config file with this value') do |mapsize|
		override_mapsize = mapsize.to_i();
	end

	opts.on('-p perlin_size', 'Forces the code to overwrite the perlin size in the config file with this value') do |perlin_size|
		override_perlinsize = perlin_size.to_i();
	end

	opts.on('-l', 'Forces the code to ignore lighting config in the defined config file') do
		no_lights = true;
	end

	opts.on('-h', '--help', 'Displays Help') do
		puts opts
		exit
	end
end
parser.parse!

$config_hash = JSON.parse(File.read(config_file_str))

if use_perlin_store then
	$config_hash["perlin_noise"]["use_store"] = true
end
if output_file_str != "undefined"
	$config_hash["core"]["output_file"] = output_file_str
end
if no_river_opts then
	$config_hash["road"]["river"]["make_river"] = false
end
if override_mapsize > 0 then
	$config_hash["core"]["map_size"][0] = override_mapsize
	$config_hash["core"]["map_size"][1] = override_mapsize
end
if override_perlinsize > 0 then
	$config_hash["perlin_noise"]["size"] = override_perlinsize
end
if no_lights then
	$config_hash["lighting"]["draw_lights"] = false
end

puts "\n"
puts "Running Random Biome Generator for Dungeondraft."
puts "Config file used: #{config_file_str}"
puts "Config name: #{$config_hash["header"]["config_name"]}"
puts "Output Filename: #{$config_hash["core"]["output_file"]}"

if $config_hash["perlin_noise"]["use_store"]
	puts "Using Perlin Data Stored in: #{$config_hash["perlin_noise"]["store_file"]}"
end

if $config_hash["core"]["map_size"][0] != $config_hash["core"]["map_size"][1]
	puts "Defined map size, #{$config_hash["core"]["map_size"][0]} x #{$config_hash["core"]["map_size"][1]}, is not square. Exiting."
end

puts "Generating Map of Size: #{$config_hash["core"]["map_size"][0]} x #{$config_hash["core"]["map_size"][1]}"
puts

$base_hash = JSON.parse(File.read($config_hash["core"]["baseline_map_file"]))
$next_node_id = 0
update_core_map_hash()

if !$config_hash["perlin_noise"]["use_store"] then
    pixels = createperlin($config_hash["perlin_noise"]["size"],$config_hash["perlin_noise"]["granularity"],$config_hash["perlin_noise"]["octaves"],$config_hash["perlin_noise"]["permutation_size"])
    store_perlin(pixels, $config_hash["perlin_noise"]["store_file"])
else
    pixels = retrieve_perlin($config_hash["perlin_noise"]["store_file"])
	$config_hash["perlin_noise"]["size"] = pixels.length()
end

if $config_hash["perlin_noise"].has_key?('modify')
    if $config_hash["perlin_noise"]["modify"]["modify_perlin"] && $config_hash["perlin_noise"]["modify"]["type"] == "Island"
        modify_perlin_island(pixels)
    end
end
simple_perlin_map = simplifyperlin(pixels)
    
scatter_trees(pixels)
scatter_clumps(pixels)
scatter_objects(pixels)

if $config_hash["terrain"]["make_terrain"]
    terrain = make_terrain(simple_perlin_map)	
end

if $config_hash.has_key?('road')
	if $config_hash["road"]["make_road"]
		make_road(simple_perlin_map,terrain)
	end
end

if $config_hash["perlin_noise"].has_key?('modify')
    if $config_hash["perlin_noise"]["modify"]["modify_perlin"] && $config_hash["perlin_noise"]["modify"]["type"] == "Canyon"
        modify_perlin_canyon(simple_perlin_map)
    end
end

apply_shadows_to_trees()
apply_lighting(simple_perlin_map)

if $config_hash["perlin_noise"]["output_perlin_ppm"]
	outputppm($config_hash["perlin_noise"]["output_perlin_ppm_filename"],simple_perlin_map,$config_hash["core"]["map_size"][0]*4,$config_hash["core"]["map_size"][1]*4,$config_hash["perlin_noise"]["granularity"])
end

if $config_hash.has_key?('swamp')
	if $config_hash["swamp"]["make_swamp"]
		make_water_areas(simple_perlin_map)
	end
end

if $config_hash.has_key?('hills')
	if $config_hash["hills"]["make_hills"]
		make_hills(simple_perlin_map)
	end
end

draw_terrain(terrain,false,$config_hash["core"]["map_size"][0],$config_hash["core"]["map_size"][1])

$base_hash['world']['next_node_id'] = $next_node_id.to_s(16)
File.open($config_hash["core"]["output_file"], "wb") { |file| file.puts JSON.pretty_generate($base_hash) }
puts "Dungeondraft file created: #{$config_hash["core"]["output_file"]}.\n\n\n"
